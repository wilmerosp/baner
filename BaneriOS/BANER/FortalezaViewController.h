//
//  VerNecesidadViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"

@interface FortalezaViewController : UIViewController<SOAPToolDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UITextViewDelegate,UITextFieldDelegate>{
    IBOutlet UIPickerView *ListFortaleza;
    NSOrderedSet * fortalezaArray;
     NSOrderedSet * fortalezaIdArray;
    NSOrderedSet * respuestaArray;
    NSOrderedSet * validarArray;
    IBOutlet UITextField *txtDocumento;
    NSTimer * tiempo;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
}
@property (nonatomic, strong) NSString *fortaleza;
@property (nonatomic, strong) NSString *fortalezaId;
@property (nonatomic, strong) UILabel *salto;
@property (strong, nonatomic) IBOutlet UITextView *txtViewDescripcion;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

- (IBAction)cbRegistro:(id)sender;

@end

//
//  RegNecesidadViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"
#import "ViewController.h"
@interface RegNecesidadViewController : UIViewController<SOAPToolDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    IBOutlet UIView * deptoViewContenedor;
    IBOutlet UIPickerView * deptoPicker;
    NSMutableOrderedSet * deptoArray;
    NSOrderedSet * DocumentArray;
    NSOrderedSet * idDeptoArray;
    NSOrderedSet * idDocumentArray;
    NSOrderedSet * validarArray;
    NSMutableOrderedSet *PobladoArray;
    NSMutableOrderedSet * ciudadArray;
    NSOrderedSet * idCiudadArray;
    NSOrderedSet *idPobladoArray;
    NSString * SegNombre;
    NSString * SegApellido;
    IBOutlet UITextField *txtNombre;
    IBOutlet UITextField *txtDireccion;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtNumDocumento;
    IBOutlet UITextField *txtApellido;
    IBOutlet UITextField *txtTipoDocumento;
    IBOutlet UITextField *txtDepto;
    IBOutlet UITextField *txtTelefono;
    NSTimer * tiempo;
    IBOutlet UITextField *txtCentroPoblado;
    
    IBOutlet UITextField *txtMunicipio;
   
}


@property (nonatomic, strong) NSString *necesidadId;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UILabel *lblNecesidad;
@property (nonatomic, strong) NSString *necesidad;
@property (nonatomic, strong) NSString *observacion;
@property (nonatomic, strong) NSString *documento;
- (IBAction)cbMunicipio:(id)sender;
- (IBAction)cbCentroPoblado:(id)sender;


//- (IBAction)Cerrar:(id)sender;
- (IBAction)cbDocumento:(id)sender;

- (IBAction)cbDepto:(id)sender;
- (IBAction)cbRegistro:(id)sender;

@end

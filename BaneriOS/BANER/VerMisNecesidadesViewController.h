//
//  VerMisNecesidadesViewController.h
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>

@interface VerMisNecesidadesViewController : UIViewController
@property (strong,nonatomic) NSString *Estado;
@property (strong,nonatomic) NSString *Observacion;
@property (strong,nonatomic) NSString *Respuesta;
@property (strong,nonatomic) NSString *Docum;
@property (strong,nonatomic) NSArray *neceArray;
@property (strong,nonatomic) NSArray *obcArray;
@property (strong,nonatomic) NSArray *respeArray;

@property (weak, nonatomic) IBOutlet UILabel *lblEstado;
@property (weak, nonatomic) IBOutlet UILabel *lblObservacion;
@property (weak, nonatomic) IBOutlet UILabel *lblRespuesta;
- (IBAction)cbregresar:(id)sender;

@end

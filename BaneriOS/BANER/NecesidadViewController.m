//
//  VerNecesidadViewController.m
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "NecesidadViewController.h"
#import "SYSoapTool.h"
#import "RegNecesidadViewController.h"

@interface NecesidadViewController (){
    //apuntador a clase conexion / temporales de validacion
    SYSoapTool *soapTool;
    NSString *seleccion;
    int cont;
    NSInteger temp;NSInteger tempup;
}

@end

@implementation NecesidadViewController
@synthesize necesidad;
@synthesize necesidadId;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _txtViewDescripcion.delegate=self;
    
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 400)];
    
//Objetos de interaccion a clase de conexion
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    cont=0;
    temp=0;
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerListadoNecesidad" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    //temporizador que da un tiempo de espera de conexion
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    cbDocumento.delegate=self;
    [activityIndicator startAnimating];
}

-(void)viewDidLayoutSubviews{
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 400)];
}

 //funcion encargada de ocultar el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtViewDescripcion resignFirstResponder];
    [cbDocumento resignFirstResponder];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    [super touchesBegan:touches withEvent:event];
}

//funcion encargado de ocultar el teclado
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_txtViewDescripcion resignFirstResponder];
    [cbDocumento resignFirstResponder];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];

    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [_txtViewDescripcion resignFirstResponder];
    [cbDocumento resignFirstResponder];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}

//Funcion validador del temporizador de conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
        [av show];
    ListNecesidades.delegate=self;
    [ListNecesidades selectRow:3 inComponent:0 animated:YES];
     //pausa del temporizador
    [tiempo invalidate];
    tiempo = nil;
    [activityIndicator stopAnimating];
    
    

}

//funcion que valida los datos que se reciven de la clase conexion al servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    [activityIndicator stopAnimating];
    NSLog(@"%@",_data);
    [tiempo invalidate];
    tiempo = nil;
    NSLog(@"RESPUESTA: %@",_data);
    //temporal que divide los tipos de datos de acuerdo a las funciones solicitadas
    //condicional para listar las fortalesas reciidas del servidor
    if (temp==0) {
        
        
        NecesidadesArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Descripcion"]];
        NecesidadesIdArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
        ListNecesidades.delegate=self;
        [ListNecesidades selectRow:3 inComponent:0 animated:YES];
        
      
    }
    //condicional para validar si una necesidad fue registrada en el servidor
    if (temp==2) {
        validarArray = [NSOrderedSet orderedSetWithArray:[_data valueForKey:@"RegistrarNecesidadResult"]];
        if([[validarArray objectAtIndex:0] isEqualToString:@"true"]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Exitoso"message: @"Los datos han sido registrados exitosamente."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            _txtViewDescripcion.text=@"";
            cbDocumento.text=@"";
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"No ha sido posible registrar la necesidad, confirme los datos y su conexión a internet."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
        }
        }else if(temp==1){
            respuestaArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"ValidarUsuarioResult"]];
            if ([[respuestaArray objectAtIndex:0]isEqualToString:@"NO"]) {
                [self performSegueWithIdentifier:@"regNecesidad" sender:nil];
            }else if ([[respuestaArray objectAtIndex:0]isEqualToString:@"OK"]){
                NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoNecesidad",@"observacion",@"latitud",@"longitud",@"cedula",nil];
                NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:necesidadId,_txtViewDescripcion.text,@"0.0",@"0.0",cbDocumento.text, nil];
                
                [soapTool callSoapServiceWithParameters__functionName:@"RegistrarNecesidad" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
                temp=2;
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//listador de datos en el Objeto de seleccion
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (!NecesidadesArray || !NecesidadesArray.count  || NecesidadesArray[0]== [NSNull null]) {
        return 1;
    }else{

        return [NecesidadesArray count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (!NecesidadesArray || !NecesidadesArray.count  || NecesidadesArray[0]== [NSNull null]) {
        NSLog(@"No hay datos / Conexion al Servidor");
    }else{
        necesidad=[NecesidadesArray objectAtIndex:row];
        necesidadId=[NecesidadesIdArray objectAtIndex:row];
        
    }
}




-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (!NecesidadesArray || !NecesidadesArray.count  || NecesidadesArray[0]== [NSNull null]) {
        return @"No hay datos / Conexion al Servidor";
    }else{
        return [NecesidadesArray objectAtIndex:row];
    }
}


//fin listador menu desplegable


//accion boton registrar

- (IBAction)cbRegistro:(id)sender {
    if (cont>250) {
        NSLog(@"error");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Ha superado el maximo de caracteres permitidos en la Observación"delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(!necesidad){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Debe seleccionar una necesidad de la lista."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(cbDocumento.text && cbDocumento.text.length < 5){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Digite un documento valido."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(_txtViewDescripcion.text && _txtViewDescripcion.text.length < 1){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Debe registrar una descripción de la necesidad."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        //llamar validacion de usuario
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"numeroCedula",nil];
        
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:cbDocumento.text, nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ValidarUsuario" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        temp=1;
        //fin validacion
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        [activityIndicator startAnimating];
    }
    
}
//llamado remoto para registrar usuarion no existente en el servidor
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"regNecesidad"]) {
        RegNecesidadViewController *lanecesidad=[segue destinationViewController];
        lanecesidad.necesidad = necesidad;
        lanecesidad.observacion=_txtViewDescripcion.text;
        lanecesidad.documento=cbDocumento.text;
        lanecesidad.necesidadId=necesidadId;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPKRSTUVWXYZÁÉÍÓÚáéíóú "] invertedSet];
    
    NSString *filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    NSLog(@"leng: %lu",(unsigned long)textView.text.length);
    if (textView.text.length >= 350 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    return [text isEqualToString:filtered];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        NSLog(@"leng: %lu",(unsigned long)textField.text.length);
        if (textField.text.length >= 350 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
    
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789+*#ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_"] invertedSet];
        
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
}



#define k_KEYBOARD_OFFSET 210.0

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
 
        if  (self.view.frame.origin.y >= 0)
        {
            [self moveViewUp:YES];
            tempup=1;
        }


}
-(void)textViewDidBeginEditing:(UITextView *)sender{

        if  (self.view.frame.origin.y >= 0)
        {
            [self moveViewUp:YES];
            tempup=1;
        }

}

//Custom method to move the view up/down whenever the keyboard is appeared / disappeared
-(void)moveViewUp:(BOOL)bMovedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4]; // to slide the view up
    
    CGRect rect = self.view.frame;
    if (bMovedUp) {
        // 1. move the origin of view up so that the text field will come above the keyboard
        rect.origin.y -= k_KEYBOARD_OFFSET;
        

    } else {
        // revert to normal state of the view.
        tempup=0;
        rect.origin.y += k_KEYBOARD_OFFSET;

    }
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

@end

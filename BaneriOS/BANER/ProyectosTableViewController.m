//
//  ProyectosTableViewController.m
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "ProyectosTableViewController.h"
#import "SYSoapTool.h"
#import "VerProyectosViewController.h"

@interface ProyectosTableViewController (){
    //apuntador a clase conexion / temporales de validacion
    SYSoapTool *soapTool;
    NSInteger temp;NSInteger tempup;
    NSInteger iddepto;
    NSInteger idciudad;
}

@end

@implementation ProyectosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Objetos de interaccion a clase de conexion
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    //animacion lista emergende de seleccion
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    ciudadViewContenedor.frame=CGRectMake(0, 600, 320, 261);
    [UIView commitAnimations];
    
}
//funcion encargada de ocultar el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [txtCiudad resignFirstResponder];
    [txtDepto resignFirstResponder];
    ciudadViewContenedor.hidden=true;
    
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    
    [super touchesBegan:touches withEvent:event];
}
//funcion que valida los datos que se reciven de la clase conexion al servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    NSLog(@"%@",_data);
    //validador de tipos de datos recibidos de la clase de conexion
    if (temp==1) {
        deptoArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [deptoArray insertObject:@"Seleccione . . ." atIndex:0];
        idDeptoArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
    }else if(temp==0){
        ciudadArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [ciudadArray insertObject:@"Seleccione . . ." atIndex:0];
        idCiudadArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
    }else{
        _proyNombreArray=[_data valueForKey:@"Nombre"];
        _proyDescripcionArray=[_data valueForKey:@"Descripcion"];
        _proyFechaIniArray=[_data valueForKey:@"FechaInicio"];
        _proyFechaFinArray=[_data valueForKey:@"FechaFin"];
        _proyIdFinArray=[_data valueForKey:@"IdProyecto"];
        
        NSDictionary *dicProyNombre = [NSDictionary dictionaryWithObjects:_proyNombreArray forKeys:_proyIdFinArray];
        _proyNombreArray=[dicProyNombre allValues];
        NSDictionary *dicProyDesc = [NSDictionary dictionaryWithObjects:_proyDescripcionArray forKeys:_proyIdFinArray];
        _proyDescripcionArray=[dicProyDesc allValues];
        NSDictionary *dicProyFechaIni = [NSDictionary dictionaryWithObjects:_proyFechaIniArray forKeys:_proyIdFinArray];
        _proyFechaIniArray=[dicProyFechaIni allValues];
        NSDictionary *dicProyFechaFin = [NSDictionary dictionaryWithObjects:_proyFechaFinArray forKeys:_proyIdFinArray];
        _proyIdFinArray=[dicProyFechaFin allValues];
        
        [activityIndicator stopAnimating];
        [self.tabla reloadData];
        
        [self.tabla reloadData];
        [activityIndicator stopAnimating];
    }
    
    ciudadPicker.delegate=self;
    //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
    [tiempo invalidate];
    tiempo = nil;
    
}
//temporizador que valida la conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
    [av show];
    [tiempo invalidate];
    tiempo = nil;
}

//accion boton cerrar de la vista desplegable
/*
 - (IBAction)Cerrar:(id)sender {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.3];
 ciudadViewContenedor.frame=CGRectMake(0, 600, 320, 261);
 [UIView commitAnimations];
 ciudadViewContenedor.hidden=true;
 }
 */
//accion que muestra la lista desplegable de seleccion de municipio
- (IBAction)cbCiudad:(id)sender {
    if ([txtDepto.text isEqualToString: @"Seleccione . . ."] || [txtDepto.text isEqualToString: @""]|| [txtDepto.text isEqualToString: @" "]) {
        NSLog(@"vacio");
    }else{
        NSString *deviceType = [UIDevice currentDevice].model;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            ciudadViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            ciudadViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        
        [UIView commitAnimations];
        
        ciudadPicker.delegate=self;
        //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
        ciudadViewContenedor.hidden=false;
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoDepartamento",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idDeptoArray objectAtIndex:iddepto], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerListadoMunicipio" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        temp=0;
        if  (tempup==1)
        {
            [self moveViewUp:NO];
        }
    }
}
//accion que muestra la lista desplegable de seleccion de departamento
- (IBAction)cbDepto:(id)sender {
    NSString *deviceType = [UIDevice currentDevice].model;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
        ciudadViewContenedor.frame=CGRectMake(0, 800, 820, 861);
    }else{
        ciudadViewContenedor.frame=CGRectMake(0, 300, 320, 261);
    }
    
    [UIView commitAnimations];
    
    ciudadPicker.delegate=self;
    //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
    ciudadViewContenedor.hidden=false;
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerListadoDepartamento" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    temp=1;
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}
//accion boton de buscar proyectos al servidor
- (IBAction)cbBuscar:(id)sender {
    //validador que se allan ingresado datos para ser consultados
    if ([txtDepto.text isEqualToString: @"Seleccione . . ."] || [txtCiudad.text isEqualToString: @"Seleccione . . ."] || [txtDepto.text isEqualToString: @""] || [txtCiudad.text isEqualToString: @""]|| [txtDepto.text isEqualToString: @" "] || [txtCiudad.text isEqualToString: @" "]) {
        //alerta datos vacios o no seleccionados
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Advertencia"message: @"Por favor seleccione un departamento y municipio para generar la busqueda"delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoMunicipio",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idCiudadArray objectAtIndex:idciudad], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerProyecto" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        temp=2;
        [activityIndicator startAnimating];
    }
    ciudadViewContenedor.hidden=true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

//listador de datos en el Objeto menu desplegable
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (temp==1) {
        return [deptoArray count];
    }else {
        return [ciudadArray count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (temp==1) {
        if (!deptoArray || !deptoArray.count  || deptoArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            iddepto = row-1;
            txtDepto.text=[deptoArray objectAtIndex:row];
            txtCiudad.text=@"Seleccione . . .";
        }
    }else{
        if (!ciudadArray || !ciudadArray.count  || ciudadArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            idciudad=row-1;
            txtCiudad.text=[ciudadArray objectAtIndex:row];
        }
    }
    /*[UIView beginAnimations:nil context:NULL];
     [UIView setAnimationDuration:0.3];
     ciudadViewContenedor.frame=CGRectMake(0, 600, 320, 261);
     [UIView commitAnimations];
     ciudadViewContenedor.hidden=true;
     */
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (temp==1) {
        if (!deptoArray || !deptoArray.count  || deptoArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
            return [deptoArray objectAtIndex:row];
        }
    }else{
        if (!ciudadArray || !ciudadArray.count  || ciudadArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
            return [ciudadArray objectAtIndex:row];
        }
    }
}
//fin listador

#pragma mark - Table view data source
//listador de Proyectos en la tabla de vistas

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([_proyNombreArray count]==0) {
        NSLog(@"error");
        return 1;
    }else{
        return [_proyNombreArray count];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!_proyNombreArray || !_proyNombreArray.count  || _proyNombreArray[0]== [NSNull null]){
        NSLog(@"No hay datos");
    }else{
        [self performSegueWithIdentifier:@"proyectoResumen" sender:self];
    }
    
}
//listar los datos en el Table View
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"celdaProyecto";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSLog(@"array proyecto: %@",_proyNombreArray[0]);
    //validador en caso de no encontrar datos relacionados a la consulta en el servidor
    if (_proyNombreArray[0]== [NSNull null]) {
        cell.textLabel.text =@" ";
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Dato no encontrado"message: @"No hay proyectos en éste municipio."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        cell.textLabel.text = [_proyNombreArray objectAtIndex:indexPath.row];
        
    }
    
    return cell;
}
//enviar datos a la vista de resumen
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"proyectoResumen"]) {
        if (!_proyNombreArray || !_proyNombreArray.count  || _proyNombreArray[0]== [NSNull null]) {
            NSLog(@"No hay datos");
            [[segue destinationViewController]setNombre:@"No hay Datos"];
            [[segue destinationViewController]setDescripcion:@"No hay Datos"];
            [[segue destinationViewController]setFechaIni:@"No hay Datos"];
            [[segue destinationViewController]setFechaFin:@"No hay Datos"];
        }else{
            NSIndexPath *indexPath=nil;
            indexPath = [self.tabla indexPathForSelectedRow];
            [[segue destinationViewController]setNombre:[_proyNombreArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setProyObservArray:[_proyDescripcionArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setProyFechaInicialArray:[_proyFechaIniArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setProyFechaFinalArray:[_proyFechaFinArray objectAtIndex:indexPath.row]];
            
            [[segue destinationViewController]setDescripcion:[_proyDescripcionArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setFechaIni:[_proyFechaIniArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setFechaFin:[_proyFechaFinArray objectAtIndex:indexPath.row]];
        }
    }
}

#define k_KEYBOARD_OFFSET 210.0

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    
    if  (self.view.frame.origin.y >= 0)
    {
        [self moveViewUp:YES];
        tempup=1;
    }
    
    
}
-(void)textViewDidBeginEditing:(UITextView *)sender{
    
    if  (self.view.frame.origin.y >= 0)
    {
        [self moveViewUp:YES];
        tempup=1;
    }
    
}

//Custom method to move the view up/down whenever the keyboard is appeared / disappeared
-(void)moveViewUp:(BOOL)bMovedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4]; // to slide the view up
    
    CGRect rect = self.view.frame;
    if (bMovedUp) {
        // 1. move the origin of view up so that the text field will come above the keyboard
        rect.origin.y -= k_KEYBOARD_OFFSET;
        
        
    } else {
        // revert to normal state of the view.
        tempup=0;
        rect.origin.y += k_KEYBOARD_OFFSET;
        
    }
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end

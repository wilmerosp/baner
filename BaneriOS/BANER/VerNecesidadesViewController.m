//
//  ProyectosTableViewController.m
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "VerNecesidadesViewController.h"
#import "SYSoapTool.h"
#import "VerMisNecesidadesViewController.h"

@interface VerNecesidadesViewController (){
    //apuntadores a clase de conexion al servidor
    SYSoapTool *soapTool;
    NSInteger temp;
   
}

@end

@implementation VerNecesidadesViewController

- (void)viewDidLoad {
    //apuntadores internos y validadores
    [super viewDidLoad];
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    _txtDocumento.text=_Docum;
    _txtDocumento.delegate=self;

}

//funcion encargada de ocultar el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    [_txtDocumento resignFirstResponder];
    
   
    [super touchesBegan:touches withEvent:event];
}

//funcion retorna los datos de la clase servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    [activityIndicator stopAnimating];
    NSLog(@"RESPUESTA: %@",_data);
    [tiempo invalidate];
    tiempo = nil;
    _misNecesidadesArray=[_data valueForKey:@"NombreNecesidad"];
    _necesidadIdArray=[_data valueForKey:@"CodigoNecesidad"];
    _observacionArray=[_data valueForKey:@"Observacion"];
    _respuestaArray=[_data valueForKey:@"Respuesta"];
    NSDictionary *dicobservacion = [NSDictionary dictionaryWithObjects:_observacionArray forKeys:_necesidadIdArray];
    _observacionArray=[dicobservacion allValues];
    NSDictionary *dicRespuesta = [NSDictionary dictionaryWithObjects:_respuestaArray forKeys:_necesidadIdArray];
    _respuestaArray=[dicRespuesta allValues];
   NSDictionary *dicNecesidad = [NSDictionary dictionaryWithObjects:_misNecesidadesArray forKeys:_necesidadIdArray];
    _misNecesidadesArray=[dicNecesidad allValues];
        [activityIndicator stopAnimating];
        [self.tabla reloadData];
}


//accion boton de buscar necesidades
- (IBAction)cbBuscar:(id)sender {
    [_txtDocumento resignFirstResponder];
    //validador de datos ingresados correctamente
    if ([_txtDocumento.text isEqualToString: @""]|| ([_txtDocumento.text isEqual:@" "])) {
        NSLog(@"vacio");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Advertencia"message: @"Por favor indique un número de documento valido."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"cedula",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:_txtDocumento.text, nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerMisNecesidades" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    
        [activityIndicator startAnimating];
    
    }
}

//temporizador que valida la conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
    [av show];
    [tiempo invalidate];
    tiempo = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
//listar los datos en el table vien, inicializacion
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_misNecesidadesArray count]==0) {
        NSLog(@"error");
        return 1;
    }else{

        return [_misNecesidadesArray count];

    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"celdaNecesidad";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSLog(@"array MIs Necesidades: %@",_misNecesidadesArray[0]);
    if ( _misNecesidadesArray[0]== [NSNull null]) {
        cell.textLabel.text =@" ";
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Dato no encontrado"message: @"No se encuentra información asociada a éste documento."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        cell.textLabel.text = [_misNecesidadesArray objectAtIndex:indexPath.row];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!_misNecesidadesArray || !_misNecesidadesArray.count  || _misNecesidadesArray[0]== [NSNull null]){
        NSLog(@"No hay datos");
    }else{
        [self performSegueWithIdentifier:@"verNecesidades" sender:self];
    }
    
}

//enviar datos a la vista de resumen
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"verNecesidades"]) {
        if (!_misNecesidadesArray || !_misNecesidadesArray.count  || _misNecesidadesArray[0]== [NSNull null]) {
            NSLog(@"No hay datos");
            [[segue destinationViewController]setEstado:@"No hay Datos"];
            [[segue destinationViewController]setObservacion:@"No hay Datos"];
            [[segue destinationViewController]setRespuesta:@"No hay Datos"];
        }else{
            NSIndexPath *indexPath=nil;
            indexPath = [self.tabla indexPathForSelectedRow];
            [[segue destinationViewController]setEstado:[_misNecesidadesArray objectAtIndex:indexPath.row]];
            [[segue destinationViewController]setObservacion:[_observacionArray objectAtIndex:indexPath.row]];
            
            [[segue destinationViewController]setNeceArray:_misNecesidadesArray];
            [[segue destinationViewController]setObcArray:_observacionArray];
            [[segue destinationViewController]setRespeArray:_respuestaArray];
            [[segue destinationViewController]setDocum:_txtDocumento.text];
            
            if (!(_respuestaArray[0]== [NSNull null])) {
                [[segue destinationViewController]setRespuesta:[_respuestaArray objectAtIndex:indexPath.row]];
            }else{
                [[segue destinationViewController]setRespuesta:@"Sin respuesta"];
            }
           // [[segue destinationViewController]setRespuesta:[respuestaArray objectAtIndex:indexPath.row]];
        }
    }
}

#define MAX_LENGTH 20

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= MAX_LENGTH && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    
    if (textField.keyboardType == UIKeyboardTypeNumberPad ) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPKRSTUVWXYZ"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    return YES;
}
@end


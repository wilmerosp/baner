//
//  VerProyectosViewController.m
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "VerProyectosViewController.h"

@interface VerProyectosViewController ()

@end

@implementation VerProyectosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //recibir datos de Vista Proyectos
    self.lblNombre.text=self.Nombre;
    self.lblDescripcion.text=self.Descripcion;
    self.lblFechaIni.text=self.FechaIni;
    self.lblFechaFin.text=self.FechaFin;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


@end

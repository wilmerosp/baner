//
//  VerNecesidadesViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"


@interface VerNecesidadesViewController : UIViewController<SOAPToolDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    
    
    NSMutableArray * estadoArray;
    
    NSTimer * tiempo;

}
- (IBAction)cbBuscar:(id)sender;
//- (IBAction)Cerrar:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong,nonatomic) NSString *Docum;
@property (strong, nonatomic) NSArray * misNecesidadesArray;
@property (strong, nonatomic) NSMutableArray * necesidadIdArray;
@property (strong, nonatomic) NSArray * observacionArray;
@property (strong, nonatomic) NSArray * respuestaArray;
@property (strong, nonatomic) NSArray * nuevoArray;
@property (strong, nonatomic) IBOutlet UITextField *txtDocumento;
@end

//
//  VerZonaViewController.h
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>

@interface VerZonaViewController : UIViewController
@property (strong,nonatomic) NSString *Estado;
@property (strong,nonatomic) NSString *Observacion;
@property (strong,nonatomic) NSString *Respuesta;

@property (strong,nonatomic) NSString *Depto;
@property (strong,nonatomic) NSString *Munic;
@property (strong,nonatomic) NSArray *NecArray;
@property (strong,nonatomic) NSArray *FortArray;
@property (strong,nonatomic) NSArray *resArray;
@property (strong,nonatomic) NSArray *obArray;
@property (strong,nonatomic) NSArray *estArray;
@property (strong,nonatomic) NSArray *descArray;

@property (weak, nonatomic) IBOutlet UILabel *lblEstado;
@property (weak, nonatomic) IBOutlet UILabel *lblObservacion;


- (IBAction)cbRegresar:(id)sender;

@end


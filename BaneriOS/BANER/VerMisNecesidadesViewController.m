//
//  VerMisNecesidadesViewController.m
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "VerMisNecesidadesViewController.h"
#import "VerNecesidadesViewController.h"

@interface VerMisNecesidadesViewController ()

@end

@implementation VerMisNecesidadesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //recibir datos de Necesidades
    self.lblEstado.text=self.Estado;
    self.lblObservacion.text=self.Observacion;
    self.lblRespuesta.text=self.Respuesta;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"volverNecesidades"]) {
       
        [[segue destinationViewController]setMisNecesidadesArray:_neceArray];
        [[segue destinationViewController]setObservacionArray:_obcArray];
        [[segue destinationViewController]setRespuestaArray:_respeArray];
        [[segue destinationViewController]setDocum:_Docum];
    }
}


- (IBAction)cbregresar:(id)sender {
    [self performSegueWithIdentifier:@"volverNecesidades" sender:self];
    
}
@end
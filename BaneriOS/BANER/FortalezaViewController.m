//
//  FortalezaViewController.m
//  BANER
//
//  Created by Mauricio Bernal on 30-09-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "FortalezaViewController.h"
#import "SYSoapTool.h"
#import "RegFortalezasViewController.h"

@interface FortalezaViewController (){
    //apuntador a clase conexion / temporales de validacion
    SYSoapTool *soapTool;
    NSString *seleccion;
    NSInteger temp;NSInteger tempup;
    int cont;
}

@end

@implementation FortalezaViewController
@synthesize fortaleza;
@synthesize fortalezaId;

- (void)viewDidLoad {
    [super viewDidLoad];
    [activityIndicator startAnimating];
    _txtViewDescripcion.delegate=self;
    
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 400)];
    
    //Objetos de interaccion a clase de conexion
    
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    cont=0;
    //Solicitud lista de fortalezas
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerListadoFortaleza" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    //temporizador que da un tiempo de espera de conexion
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    temp=0;
    txtDocumento.delegate=self;
    
}

-(void)viewDidLayoutSubviews{
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 400)];
}

 //funcion encargada de ocultar el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
   
  
    [_txtViewDescripcion resignFirstResponder];
    [txtDocumento resignFirstResponder];
    
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }

    [super touchesBegan:touches withEvent:event];
}
//Funcion validador del temporizador de conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
    [av show];
    //pausa del temporizador
    [tiempo invalidate];
    tiempo = nil;
    [activityIndicator stopAnimating];
    ListFortaleza.delegate=self;
    [ListFortaleza selectRow:3 inComponent:0 animated:YES];
}

//funcion que valida los datos que se reciven de la clase conexion al servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    [activityIndicator stopAnimating];
    //pausa del temporizador
    [tiempo invalidate];
    tiempo = nil;
   NSLog(@"%@",_data);
//temporal que divide los tipos de datos de acuerdo a las funciones solicitadas
    //condicional para listar las fortalesas reciidas del servidor
    if (temp==0) {
        fortalezaArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Descripcion"]];
        fortalezaIdArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"IdFortaleza"]];
        ListFortaleza.delegate=self;
        [ListFortaleza selectRow:3 inComponent:0 animated:YES];
      
    }
    //condicional para validar si una fortaleza fue registrada en el servidor
    if (temp==2) {
        validarArray = [NSOrderedSet orderedSetWithArray:[_data valueForKey:@"RegistrarFortalezaResult"]];
        if([[validarArray objectAtIndex:0] isEqualToString:@"true"]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Exitoso"message: @"Los datos han sido registrados exitosamente."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            txtDocumento.text=@"";
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"No ha sido posible registrar la fortaleza, confirme los datos y su conexión a internet."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
        }

        }else if(temp==1){
            respuestaArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"ValidarUsuarioResult"]];
            if ([[respuestaArray objectAtIndex:0]isEqualToString:@"NO"]) {
                [self performSegueWithIdentifier:@"regFortaleza" sender:nil];
            }else if ([[respuestaArray objectAtIndex:0]isEqualToString:@"OK"]){
                NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoFortaleza",@"observacion",@"cedula",nil];
                NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:fortalezaId,_txtViewDescripcion.text,txtDocumento.text, nil];
                
                [soapTool callSoapServiceWithParameters__functionName:@"RegistrarFortaleza" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
                tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
                temp=2;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//listador de datos en el Objeto menu desplegable
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (!fortalezaArray || !fortalezaArray.count  || fortalezaArray[0]== [NSNull null]) {
        return 1;
    }else{
        return [fortalezaArray count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (!fortalezaArray || !fortalezaArray.count  || fortalezaArray[0]== [NSNull null]) {
        NSLog(@"No hay datos / Conexion al Servidor");
    }else{
        fortaleza=[fortalezaArray objectAtIndex:row];
        fortalezaId=[fortalezaIdArray objectAtIndex:row];
    }
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (!fortalezaArray || !fortalezaArray.count  || fortalezaArray[0]== [NSNull null]) {
        return @"No hay datos / Conexion al Servidor";
    }else{
        return [fortalezaArray objectAtIndex:row];
    }
    
}
//fin listador menu desplegable

//accion boton registrar
- (IBAction)cbRegistro:(id)sender {
    if (cont>250) {
       //advertencia en caso que la observacion supere los 250 caracteres
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Ha superado el maximo de caracteres permitidos en la Descripción"delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(!fortaleza){
        //advertencia en caso que no se alla seleccionado una fortaleza de la lista
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Debe seleccionar una fortaleza de la lista."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(txtDocumento.text && txtDocumento.text.length < 5){
        //advertencia en caso de no registrarse un numero de documento
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Digite un documento valido."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else if(_txtViewDescripcion.text && _txtViewDescripcion.text.length < 1){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"Debe registrar una descripción de la fortaleza."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        //llamar validacion de usuario
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"numeroCedula",nil];
      
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:txtDocumento.text, nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ValidarUsuario" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        
        temp=1;
        //fin validacion
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        [activityIndicator startAnimating];
    }
}

//llamado remoto para registrar usuarion no existente en el servidor
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"regFortaleza"]) {
        RegFortalezasViewController *lafortaleza=[segue destinationViewController];
        lafortaleza.fortaleza = fortaleza;
        lafortaleza.observacion=_txtViewDescripcion.text;
        lafortaleza.documento=txtDocumento.text;
        lafortaleza.fortalezaId=fortalezaId;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.keyboardType == UIKeyboardTypeNumberPad ) {
        NSLog(@"leng: %lu",(unsigned long)textField.text.length);
        if (textField.text.length >= 20 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789+*#ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    return YES;
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPKRSTUVWXYZáéíóúÁÉÍÓÚ "] invertedSet];
    
    NSString *filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    NSLog(@"leng: %lu",(unsigned long)textView.text.length);
    if (textView.text.length >= 250 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    return [text isEqualToString:filtered];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}

#define k_KEYBOARD_OFFSET 210.0

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    
    if  (self.view.frame.origin.y >= 0)
    {
        [self moveViewUp:YES];
        tempup=1;
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [_txtViewDescripcion resignFirstResponder];
    [txtDocumento resignFirstResponder];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)sender{
    
    if  (self.view.frame.origin.y >= 0)
    {
        [self moveViewUp:YES];
        tempup=1;
    }
    
}

//Custom method to move the view up/down whenever the keyboard is appeared / disappeared
-(void)moveViewUp:(BOOL)bMovedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4]; // to slide the view up
    
    CGRect rect = self.view.frame;
    if (bMovedUp) {
        // 1. move the origin of view up so that the text field will come above the keyboard
        rect.origin.y -= k_KEYBOARD_OFFSET;
        
        
    } else {
        // revert to normal state of the view.
        tempup=0;
        rect.origin.y += k_KEYBOARD_OFFSET;
        
    }
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
@end

//
//  ZonaViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "ZonaViewController.h"
#import "SYSoapTool.h"
#import "VerZonaViewController.h"
@interface ZonaViewController (){
    //apuntadores a clase servidor y temporales
    SYSoapTool *soapTool;
    NSInteger temp;
    NSInteger idSegue;
    NSInteger iddepto;
    NSInteger idciudad;
    NSInteger idrow;
}

@end

@implementation ZonaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //objetos de conexion a clase servidor
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    //animacion de vista emergente
 
}

//funcion que oculta el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    [txtDepto resignFirstResponder];
    [txtCiudad resignFirstResponder];
    ciudadViewContenedor.hidden=true;
    
    [super touchesBegan:touches withEvent:event];
}

//funcion de retorno de datos de la clase de conexion al servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    NSLog(@"%@",_data);
    [tiempo invalidate];
    tiempo = nil;

    if (temp==1) {
        //deptoArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        deptoArray= [NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [deptoArray insertObject:@"Seleccione . . ." atIndex:0];
        idDeptoArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
        NSLog(@"%@",deptoArray);
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        NSString *deviceType = [UIDevice currentDevice].model;
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            ciudadViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            ciudadViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        [UIView commitAnimations];
        
        ciudadPicker.delegate=self;
        //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
        ciudadViewContenedor.hidden=false;
    }else if(temp==0){
        ciudadArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [ciudadArray insertObject:@"Seleccione . . ." atIndex:0];
        idCiudadArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        NSString *deviceType = [UIDevice currentDevice].model;
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            ciudadViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            ciudadViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        [UIView commitAnimations];
        
        ciudadPicker.delegate=self;
        //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
        ciudadViewContenedor.hidden=false;
        
    }else{
        
        
        _fortNombreArray=[_data valueForKey:@"NombreFortaleza"];
        _fortDescripcionArray=[_data valueForKey:@"Descripcion"];
        _neceNombreArray=[_data valueForKey:@"NombreNecesidad"];
        _neceObservaArray=[_data valueForKey:@"Observacion"];
        _fortIdArray=[_data valueForKey:@"Id"];
        _necIdArray=[_data valueForKey:@"CodigoNecesidad"];
        
        NSDictionary *dicFortNombre = [NSDictionary dictionaryWithObjects:_fortNombreArray forKeys:_fortIdArray];
        _fortNombreArray=[dicFortNombre allValues];
        NSDictionary *dicFortDescr = [NSDictionary dictionaryWithObjects:_fortDescripcionArray forKeys:_fortIdArray];
        _fortDescripcionArray=[dicFortDescr allValues];
        NSDictionary *dicNeceNombre = [NSDictionary dictionaryWithObjects:_neceNombreArray forKeys:_necIdArray];
        _neceNombreArray=[dicNeceNombre allValues];
        NSDictionary *dicNeceobcerv = [NSDictionary dictionaryWithObjects:_neceObservaArray forKeys:_necIdArray];
        _neceObservaArray=[dicNeceobcerv allValues];
        
         NSMutableArray *fortArray = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_fortNombreArray count]; i++) {
         id obj = [_fortNombreArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [fortArray addObject:obj];
         }
         }
         
         _fortNombreArray=fortArray;
         
         NSMutableArray *necArray = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_neceNombreArray count]; i++) {
         id obj = [_neceNombreArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [necArray addObject:obj];
         }
         }
         _neceNombreArray=necArray;
         
         if ((!_fortNombreArray || !_fortNombreArray.count  || _fortNombreArray[0]== [NSNull null])&&(!_neceNombreArray || !_neceNombreArray.count  || _neceNombreArray[0]== [NSNull null])) {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Dato no encontrado"message: @"No hay Necesidades/Fortalezas registradas en el municipio seleccionado."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
         [alert show];
         }
         
         NSMutableArray *necObEsArray = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_neceObservaArray count]; i++) {
         id obj = [_neceObservaArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [necObEsArray addObject:obj];
         }
         }
         _neceObservaArray=necObEsArray;
         
         NSMutableArray *necDesEsArray = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_fortDescripcionArray count]; i++) {
         id obj = [_fortDescripcionArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [necDesEsArray addObject:obj];
         }
         }
         _fortDescripcionArray=necDesEsArray;
        
         NSMutableArray *mutIdarray = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_necIdArray count]; i++) {
         id obj = [_necIdArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [mutIdarray addObject:obj];
         }
         }
         _necIdArray=mutIdarray;
         NSMutableArray *mutIdarray2 = [[NSMutableArray alloc]init];
         for (int i = 0; i < [_fortIdArray count]; i++) {
         id obj = [_fortIdArray objectAtIndex:i];
         if (![obj  isKindOfClass:[NSNull class]]) { // or if (![obj  isKindOfClass:[[NSNull null]class]]) {
         [mutIdarray2 addObject:obj];
         }
         }
         _fortIdArray=mutIdarray;
     
        idrow=0;
        [self.tabla reloadData];
        //[self.tabla2 reloadData];
    }
    
    ciudadPicker.delegate=self;
    //[ciudadPicker selectRow:3 inComponent:0 animated:YES];
    
}

//temporizador encargado de validar la conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
    [av show];
    [tiempo invalidate];
    tiempo = nil;
}


//accion que muestra la lista de municipios de la clase servidor
- (IBAction)cbCiudad:(id)sender {
    if ([txtDepto.text isEqualToString: @"Seleccione . . ."] || [txtDepto.text isEqualToString: @""]|| ([txtDepto.text isEqual:@" "])) {
        NSLog(@"vacio");
    }else{
        
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoDepartamento",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idDeptoArray objectAtIndex:iddepto], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerListadoMunicipio" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        temp=0;
    }
}

//accion que muestra la lista de munisipios de la clase servidor
- (IBAction)cbDepto:(id)sender {
    
    
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerListadoDepartamento" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    temp=1;
}

//accion que genera el empaquetamiento de la busqueda a la clase servidor 
- (IBAction)cbBuscar:(id)sender {
    [txtCiudad resignFirstResponder];
    if ([txtDepto.text isEqualToString: @"Seleccione . . ."] || [txtCiudad.text isEqualToString: @"Seleccione . . ."]|| [txtDepto.text isEqualToString: @""] || [txtCiudad.text isEqualToString: @""]|| ([txtDepto.text isEqual:@" "])|| ([txtCiudad.text isEqual:@" "])) {
        NSLog(@"vacio");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Advertencia"message: @"Por favor seleccione un departamento y municipio para generar la busqueda"delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigomunicipio",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idCiudadArray objectAtIndex:idciudad], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerZona" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
        temp=2;
    }
    ciudadViewContenedor.hidden=true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//listador de datos en el selector
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (temp==1) {
        return [deptoArray count];
    }else {
        return [ciudadArray count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (temp==1) {
        iddepto = row-1;
        txtDepto.text=[deptoArray objectAtIndex:row];
        txtCiudad.text=@"Seleccione . . .";
    }else{
        idciudad=row-1;
        txtCiudad.text=[ciudadArray objectAtIndex:row];
    }
    /*
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    ciudadViewContenedor.frame=CGRectMake(0, 600, 320, 261);
    [UIView commitAnimations];
    ciudadViewContenedor.hidden=true;
     */
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (temp==1) {
        if (!deptoArray || !deptoArray.count  || deptoArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
            
            return [deptoArray objectAtIndex:row];
        }
    }else{
        if (!ciudadArray || !ciudadArray.count  || ciudadArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
          
            return [ciudadArray objectAtIndex:row];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if (idrow == 0) { //table view one
        if ([_fortNombreArray count]==0) {
            NSLog(@"error");
            idrow=1;
            [self.tabla2 reloadData];
            return 1;
        }else{
            idrow=1;
            [self.tabla2 reloadData];
            return [_fortNombreArray count];
            
        }
    }else {
        if ([_neceNombreArray count]==0) {
            NSLog(@"error");
            idrow=0;
            return 1;
        }else{
            idrow=0;
            return [_neceNombreArray count];        }
    
    }

    
}

//listar los datos en el Table View
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellFortaleza;
    static NSString *CellNecesidad;
    if (tableView == _tabla) { //table view one
        CellFortaleza = @"celdaFortaleza";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellFortaleza forIndexPath:indexPath];
        
        if (!_fortNombreArray || !_fortNombreArray.count  || _fortNombreArray[0]== [NSNull null]) {
            cell.textLabel.text =@" ";
            
            return cell;
        }else{
            //indexPath = [self.tabla indexPathForSelectedRow];
            cell.textLabel.text = [_fortNombreArray objectAtIndex:indexPath.row];
            return cell;
        }
    }else{
        CellNecesidad = @"celdaNecesidad";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellNecesidad forIndexPath:indexPath];
         NSLog(@"%ld",(long)indexPath.row);
        if ( !_neceNombreArray || !_neceNombreArray.count  || _neceNombreArray[0]== [NSNull null]/*|| !_neceNombreArray.count==(indexPath.row-1)*/) {
            cell.textLabel.text =@" ";
            return cell;
            
        }else{
            //indexPath = [indexPathForCell:self.tabla2];
            cell.textLabel.text = [_neceNombreArray objectAtIndex:indexPath.row];
            NSLog(@"%ld",(long)indexPath.row);

            return cell;
        }

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   if (tableView == _tabla) {
        if (!_fortNombreArray || !_fortNombreArray.count  || _fortNombreArray[0]== [NSNull null]){
            NSLog(@"No hay datos");
        }else{
            idSegue=0;
            [self performSegueWithIdentifier:@"verZona" sender:self];
        }
   }else{
       if (!_neceNombreArray || !_neceNombreArray.count  || _neceNombreArray[0]== [NSNull null])
        {
            NSLog(@"No hay datos");
        }else{
            idSegue=1;
            [self performSegueWithIdentifier:@"verZona" sender:self];
        }
    
    }

}
//enviar datos a la vista de resumen
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"verZona"]) {
        if ((!_neceNombreArray || !_neceNombreArray.count  || _neceNombreArray[0]== [NSNull null])&&(!_fortNombreArray || !_fortNombreArray.count  || _fortNombreArray[0]== [NSNull null])) {
            NSLog(@"No hay datos");
            [[segue destinationViewController]setEstado:@"No hay Datos"];
            [[segue destinationViewController]setObservacion:@"No hay Datos"];
          
        }else{
           
            if (idSegue == 0) {
                NSIndexPath *indexPath=nil;
                indexPath = [self.tabla indexPathForSelectedRow];
                [[segue destinationViewController]setEstado:[_fortNombreArray objectAtIndex:indexPath.row]];
                [[segue destinationViewController]setObservacion:[_fortDescripcionArray objectAtIndex:indexPath.row]];
                
            }else{
                NSIndexPath *indexPath=nil;
                indexPath = [self.tabla2 indexPathForSelectedRow];
                [[segue destinationViewController]setEstado:[_neceNombreArray objectAtIndex:indexPath.row]];
                [[segue destinationViewController]setObservacion:[_neceObservaArray objectAtIndex:indexPath.row]];
            }
            
            
            [[segue destinationViewController]setDepto:[txtDepto text]];
            [[segue destinationViewController]setMunic:[txtCiudad text]];
            [[segue destinationViewController]setNecArray:_neceNombreArray];
            [[segue destinationViewController]setFortArray:_fortNombreArray];
            [[segue destinationViewController]setResArray:_neceRespArray];
            [[segue destinationViewController]setObArray:_neceObservaArray];
            [[segue destinationViewController]setEstArray:_neceEstadoArray];
            [[segue destinationViewController]setDescArray:_fortDescripcionArray];
          
        }
    }
}
@end
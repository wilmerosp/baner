//
//  SYXmlParser.h
//  BestSoapTool
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SYXmlParser : NSObject <NSXMLParserDelegate>{
	NSURL *URL;
	NSData *dataToParse;
    NSArray *tags;
	NSXMLParser *parser;
	NSString *currentParsedData;
    NSString *currentElement;
	NSMutableString *currentString;
	NSMutableDictionary *item;
	NSMutableArray *theDataArray;
}
- (id)initWithURL:(NSURL *)url;
- (id)initWithData:(NSData *)data;

@property(nonatomic,retain)NSMutableArray *theDataArray;

-(void)startParser;

@end

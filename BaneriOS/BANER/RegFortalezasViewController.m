//
//  RegNecesidadViewController.m
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "RegFortalezasViewController.h"
#import "SYSoapTool.h"

@interface RegFortalezasViewController(){
    //apuntador a clase conexion / temporales de validacion
    SYSoapTool *soapTool;
    NSInteger temp;NSInteger tempup;
    NSInteger iddoc;
    NSInteger iddepto;
    NSInteger idciudad;
    NSInteger idPoblado;
}


@end

@implementation RegFortalezasViewController
//apuntadores que reciben datos de la vista necesidad
@synthesize lblFortaleza;
@synthesize fortaleza;
@synthesize fortalezaId;
@synthesize observacion;
@synthesize documento;


 //funcion encargado de ocultar el teclado
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [txtNombre resignFirstResponder];
    [txtDireccion resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtApellido resignFirstResponder];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView commitAnimations];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}

//funcion encargada de ocultar el teclado
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [txtNombre resignFirstResponder];
    [txtDireccion resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtApellido resignFirstResponder];
    [txtTelefono resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtDepto resignFirstResponder];
    [txtTipoDocumento resignFirstResponder];
    deptoViewContenedor.hidden=true;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
    [UIView commitAnimations];
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    [super touchesBegan:touches withEvent:event];
}
//funcion que valida los datos que se reciven de la clase conexion al servidor
-(void)retriveFromSYSoapTool:(NSMutableArray *)_data{
    NSLog(@"%@",_data);
    //validador de tipos de datos recibidos de la clase de conexion
    [self.view bringSubviewToFront:deptoViewContenedor];
    if (temp==0) {
        deptoArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [deptoArray insertObject:@"Seleccione . . ." atIndex:0];
        idDeptoArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
        NSString *deviceType = [UIDevice currentDevice].model;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            deptoViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        
        [UIView commitAnimations];
        
        deptoPicker.delegate=self;
        //[deptoPicker selectRow:3 inComponent:0 animated:YES];
        deptoViewContenedor.hidden=false;
    }else if(temp==1){
        DocumentArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Descripcion"]];
        idDocumentArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Id"]];
        NSString *deviceType = [UIDevice currentDevice].model;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            deptoViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        
        [UIView commitAnimations];
        
        deptoPicker.delegate=self;
        //[deptoPicker selectRow:3 inComponent:0 animated:YES];
        deptoViewContenedor.hidden=false;
    }else if(temp==3){
        validarArray = [NSOrderedSet orderedSetWithArray:[_data valueForKey:@"RegistrarFortalezaResult"]];
        //validador si la fortaleza fue guardada en el servidor
        if([[validarArray objectAtIndex:0] isEqualToString:@"true"]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Exitoso"message: @"Los datos han sido registrados exitosamente"delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            txtNumDocumento.text=@"";txtDepto.text=@"Seleccione . . .";txtDireccion.text=@"";txtApellido.text=@"";txtNumDocumento.text=@"";txtEmail.text=@"";txtNombre.text=@"";txtNumDocumento.text=@"";txtTipoDocumento.text=@"Seleccione . . .";txtTelefono.text=@"";
            [self performSegueWithIdentifier:@"volMenu" sender:nil];
            
        }else{
            //validador de datos ingresados correctamente
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"No ha sido posible registrar la fortaleza, confirme los datos y su conexion a internet."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
        }
    }else if(temp==2){
        //validador si el usuario fue registrado exitosamente el temporizador se encarga de los tiempos para cada validador
        validarArray = [NSOrderedSet orderedSetWithArray:[_data valueForKey:@"RegistrarUsuarioResult"]];
        if ([[validarArray objectAtIndex:0] isEqualToString:@"true"]){
            NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoNecesidad",@"observacion",@"latitud",@"longitud",@"cedula",nil];
            NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:fortalezaId,observacion,@"0.0",@"0.0",txtNumDocumento.text, nil];
            
            [soapTool callSoapServiceWithParameters__functionName:@"RegistrarFortaleza" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
            temp=3;
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"No ha sido posible registrar la Fortaleza, confirme los datos y su conexion a internet."delegate: nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil];
            [alert show];
            
        }
        //captura de datos de municipio para la lista de seleccion
    }else if(temp==4){
        municipioArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"Nombre"]];
        [municipioArray insertObject:@"Seleccione . . ." atIndex:0];
        idMunicipioArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Codigo"]];
        NSString *deviceType = [UIDevice currentDevice].model;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            deptoViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        
        [UIView commitAnimations];
        
        deptoPicker.delegate=self;
        //[deptoPicker selectRow:3 inComponent:0 animated:YES];
        deptoViewContenedor.hidden=false;
    }else if(temp==5){
        PobladoArray=[NSMutableOrderedSet orderedSetWithArray:[_data valueForKey:@"NombreCentroPoblado"]];
        [PobladoArray insertObject:@"Seleccione . . ." atIndex:0];
        idPobladoArray=[NSOrderedSet orderedSetWithArray:[_data valueForKey:@"Id"]];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        NSString *deviceType = [UIDevice currentDevice].model;
        if ([deviceType isEqual:@"iPad"]||[deviceType isEqual:@"iPad Simulator"]) {
            deptoViewContenedor.frame=CGRectMake(0, 800, 820, 861);
        }else{
            deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
        }
        [UIView commitAnimations];
        deptoPicker.delegate=self;
        deptoViewContenedor.hidden=false;
        
    }
   
    deptoPicker.delegate=self;
     [deptoPicker selectRow:0 inComponent:0 animated:YES];
    [tiempo invalidate];
    tiempo = nil;
    
}

//funcion que retorna los servicios a menu principal
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"volMenu"]){
        ViewController *controller = (ViewController *)segue.destinationViewController;
        controller.isSomethingEnabled = 0;
      
        deptoPicker.delegate=self;
         //[deptoPicker selectRow:3 inComponent:0 animated:YES];
        [tiempo invalidate];
        tiempo = nil;
    }
}

-(void)viewDidLayoutSubviews{
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 420)];
}

/*-(void) viewDidAppear:(BOOL)animated
{
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 400)];
}*/

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(300, 420)];
    self.lblFortaleza.text=fortaleza;
    txtDocumento.text=documento;
    
    // delegates de validacion
    
    txtNumDocumento.delegate=self;txtDireccion.delegate=self;txtEmail.delegate=self;txtMunicipio.delegate=self;
    txtApellido.delegate=self;txtNombre.delegate=self;txtTelefono.delegate=self;
    txtDepto.delegate=self;txtNumDocumento.delegate=self;txtDocumento.delegate=self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    //aniacion de la vista emergente
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    deptoViewContenedor.frame=CGRectMake(0, 300, 320, 261);
    [UIView commitAnimations];

    
}

-(void)dismissKeyboard {
    //ocultar teclado
    [txtNombre resignFirstResponder];
    [txtDireccion resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtApellido resignFirstResponder];
    [txtTelefono resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtDepto resignFirstResponder];
    [txtTipoDocumento resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    deptoViewContenedor.frame=CGRectMake(0, 600, 320, 261);
    [UIView commitAnimations];
    deptoViewContenedor.hidden=true;
}

//temporizador que se encarga de validar la conexion al servidor
-(void)comienza:(NSTimer *) elContador{
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"no se tiene acceso al servidor, verifique su conexión" delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
    [av show];
    [tiempo invalidate];
    tiempo = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//accion boton de selecion de Municipio
- (IBAction)cbMunicipio:(id)sender {
    if ([txtDepto.text isEqualToString:@"Seleccione . . ."]|| [txtDepto.text isEqualToString: @""] || [txtDepto.text isEqualToString: @" "]) {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"Debe seleccionar primero el Departamento." delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
        [av show];
    }else{
        
        soapTool = [[SYSoapTool alloc]init];
        soapTool.delegate = self;
        temp=4;
        // ocultar teclado
        [txtNombre resignFirstResponder];
        [txtDireccion resignFirstResponder];
        [txtEmail resignFirstResponder];
        [txtNumDocumento resignFirstResponder];
        [txtApellido resignFirstResponder];
        [txtTelefono resignFirstResponder];
        [txtNumDocumento resignFirstResponder];
        [txtDepto resignFirstResponder];
        [txtTipoDocumento resignFirstResponder];
         
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoDepartamento",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idDeptoArray objectAtIndex:iddepto], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerListadoMunicipio" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    }
}

//accion boton de selecion de Documento
- (IBAction)cbDocumento:(id)sender {
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    //ocultar teclado
    [txtNombre resignFirstResponder];
    [txtDireccion resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtApellido resignFirstResponder];
    [txtTelefono resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtDepto resignFirstResponder];
    [txtTipoDocumento resignFirstResponder];
    
    temp=1;
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerTiposDocumento" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
}

//accion boton de selecion de Departamento
- (IBAction)cbDepto:(id)sender {
    soapTool = [[SYSoapTool alloc]init];
    soapTool.delegate = self;
    temp=0;
    //ocultar teclado
    [txtNombre resignFirstResponder];
    [txtDireccion resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtApellido resignFirstResponder];
    [txtTelefono resignFirstResponder];
    [txtNumDocumento resignFirstResponder];
    [txtDepto resignFirstResponder];
    [txtTipoDocumento resignFirstResponder];
    
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
    
    //llamado a clase encargada de la conexion al servidor
    [soapTool callSoapServiceWithoutParameters__functionName:@"ObtenerListadoDepartamento" wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
    tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
}

- (IBAction)cbCentroPoblado:(id)sender {
    if ([txtMunicipio.text isEqualToString:@"Seleccione . . ."]|| ([txtMunicipio.text isEqual:@""])|| ([txtMunicipio.text isEqual:@" "])) {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Advertencia" message:@"Debe seleccionar primero el Municipio." delegate:nil cancelButtonTitle:@"aceptar" otherButtonTitles:nil, nil];
        [av show];
    }else{
        soapTool = [[SYSoapTool alloc]init];
        soapTool.delegate = self;
        temp=5;
        //ocultar teclado
        [txtNombre resignFirstResponder];
        [txtDireccion resignFirstResponder];
        [txtEmail resignFirstResponder];
        [txtNumDocumento resignFirstResponder];
        [txtApellido resignFirstResponder];
        [txtTelefono resignFirstResponder];
        [txtNumDocumento resignFirstResponder];
        [txtDepto resignFirstResponder];
        [txtTipoDocumento resignFirstResponder];
        
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"codigoMunicipio",nil];
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:[idDeptoArray objectAtIndex:iddepto], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"ObtenerListadoCentroPoblado" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    }
    if  (tempup==1)
    {
        [self moveViewUp:NO];
    }
}

//accion de registro de datos
- (IBAction)cbRegistro:(id)sender {
    //validador de datos en el formulario
    if ((txtApellido.text.length==0) || (txtDepto.text.length==0)|| (txtDireccion.text.length==0)||  (txtNombre.text.length==0)|| (txtNumDocumento.text.length==0)|| (txtTelefono.text.length==0)|| ([txtTipoDocumento.text isEqual:@"Seleccione . . ."])|| ([txtDepto.text isEqual:@"Seleccione . . ."])|| ([txtTipoDocumento.text isEqual:@""])|| ([txtDepto.text isEqual:@""])|| ([txtTipoDocumento.text isEqual:@" "])|| ([txtDepto.text isEqual:@" "])|| ([txtCentroPoblado.text isEqual:@" "])) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Registro Incorrecto"message: @"La informacion de registro no puede estar vacia o con datos incorrectos."delegate: nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }else{
//empaquetado y envio de datos de registro a la clase encargada de la conexion al servidor
        NSMutableArray *tags = [[NSMutableArray alloc]initWithObjects:@"cedula",@"nombre1",@"nombre2",@"apellido1",@"apellido2",@"telefono",@"email",@"direccion",@"idTipoDocumento",@"idMunicipio",@"idCentroPoblado",nil];
        NSArray *nombre = [txtNombre.text componentsSeparatedByString:@" "];
        NSArray *apellido = [txtNombre.text componentsSeparatedByString:@" "];
//condicional para evitar error de datos vacios en el envio al servidor
        if (nombre.count==2&& !([[nombre objectAtIndex:1] isEqualToString:@""])) {
            SegNombre=nombre[1];
        }else{
            SegNombre=@" ";
        }
        if (apellido.count==2&& !([[apellido objectAtIndex:1] isEqualToString:@""])) {
            SegApellido=apellido[1];
        }else{
            SegApellido=@" ";
        }
        NSMutableArray *vars = [[NSMutableArray alloc]initWithObjects:txtNumDocumento.text,[nombre objectAtIndex:0],SegNombre,[apellido objectAtIndex:0],SegApellido,txtTelefono.text,txtEmail.text,txtDireccion.text,[idDocumentArray objectAtIndex:iddoc],[idMunicipioArray objectAtIndex:idciudad],[idPobladoArray objectAtIndex:idPoblado], nil];
        
        [soapTool callSoapServiceWithParameters__functionName:@"RegistrarUsuario" tags:tags vars:vars wsdlURL:@"http://186.154.209.213:3605/ServicioNecesidad.asmx"];
        temp=2;
        tiempo = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(comienza:) userInfo:nil repeats:NO];
    }
}


//listador de datos en el Objeto menu desplegable
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (temp==0) {
        return [deptoArray count];
    }else if(temp==1){
        return [DocumentArray count];
    }else if(temp==4){
        return [municipioArray count];
    }else{
        return [PobladoArray count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (temp==0) {
        if (!deptoArray || !deptoArray.count  || deptoArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            txtDepto.text=[deptoArray objectAtIndex:row];
            txtMunicipio.text=@"Seleccione . . .";
            iddepto=row-1;
        }
    }else if(temp==1){
        if (!DocumentArray || !DocumentArray.count  || DocumentArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            txtTipoDocumento.text=[DocumentArray objectAtIndex:row];
            iddoc=row;
        }
    }else if(temp==4){
        if (!municipioArray || !municipioArray.count  || municipioArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            txtMunicipio.text=[municipioArray objectAtIndex:row];
            idciudad=row-1;
        }
    }else {
        if (!PobladoArray || !PobladoArray.count  || PobladoArray[0]== [NSNull null]) {
            NSLog(@"No hay datos / Conexion al Servidor");
        }else{
            txtCentroPoblado.text=[PobladoArray objectAtIndex:row];
            idPoblado=row-1;
        }
    }
    /*
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    deptoViewContenedor.frame=CGRectMake(0, 600, 320, 261);
    [UIView commitAnimations];
    deptoViewContenedor.hidden=true;
     */
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (temp==0) {
        if (!deptoArray || !deptoArray.count  || deptoArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
           
            return [deptoArray objectAtIndex:row];
        }
    }else if(temp==1){
        if (!DocumentArray || !DocumentArray.count  || DocumentArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
          
            return [DocumentArray objectAtIndex:row];
        }
    }else if(temp==4){
        if (!municipioArray || !municipioArray.count  || municipioArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
            
            return [municipioArray objectAtIndex:row];
        }
    }else{
        if (!PobladoArray || !PobladoArray.count  || PobladoArray[0]== [NSNull null]) {
            return @"No hay datos / Conexion al Servidor";
        }else{
            
            return [PobladoArray objectAtIndex:row];
        }
    }
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= 50 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    
    if (textField.keyboardType == UIKeyboardTypeNumberPad ) {
        if(textField.text.length >= 20 && range.length == 0){
            return NO;
        }
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPKRSTUVWXYZáéíóúÁÉÍÓÚ"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    if (textField.keyboardType == UIKeyboardTypePhonePad ) {
        if(textField.text.length >= 20 && range.length == 0){
            return NO;
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789+*#"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnoñpqrstuvwxyzABCDEFGHIJKLMNÑOPKRSTUVWXYZáéíóúÁÉÍÓÚ*,.-@ #"] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

#define k_KEYBOARD_OFFSET 170.0


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    
    if  (self.view.frame.origin.y >= 0)
    {
        tempup=1;
        [self moveViewUp:YES];
    }
    
    
}
-(void)textViewDidBeginEditing:(UITextView *)sender{
    
    if  (self.view.frame.origin.y >= 0)
    {
        tempup=1;
        [self moveViewUp:YES];
    }
    
}

//Custom method to move the view up/down whenever the keyboard is appeared / disappeared
-(void)moveViewUp:(BOOL)bMovedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4]; // to slide the view up
    
    CGRect rect = self.view.frame;
    if (bMovedUp) {
        // 1. move the origin of view up so that the text field will come above the keyboard
        rect.origin.y -= k_KEYBOARD_OFFSET;
        deptoViewContenedor.hidden=true;
        
    } else {
        // revert to normal state of the view.
        tempup=0;
        rect.origin.y += k_KEYBOARD_OFFSET;
        
    }
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end

//
//  ZonaViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"

@interface ZonaViewController : UIViewController<SOAPToolDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>{
    
   
    IBOutlet UITextField *txtDepto;
    IBOutlet UITextField *txtCiudad;
    IBOutlet UIView * ciudadViewContenedor;
    IBOutlet UIPickerView * ciudadPicker;
    NSMutableOrderedSet * ciudadArray;
    NSMutableOrderedSet * deptoArray;
    NSOrderedSet * idDeptoArray;
    NSOrderedSet * idCiudadArray;
    NSArray *tempArray;
    
    NSTimer * tiempo;

}
- (IBAction)cbDepto:(id)sender;
- (IBAction)cbBuscar:(id)sender;
//- (IBAction)Cerrar:(id)sender;
- (IBAction)cbCiudad:(id)sender;

@property (strong,nonatomic) NSString *Depto;
@property (strong,nonatomic) NSString *Munic;
@property (strong,nonatomic) NSArray * neceNombreArray;
@property (strong,nonatomic) NSArray * fortNombreArray;
@property (strong,nonatomic) NSArray * fortIdArray;
@property (strong,nonatomic) NSArray * necIdArray;
@property (strong,nonatomic) NSArray * neceEstadoArray;
@property (strong,nonatomic) NSArray * neceObservaArray;
@property (strong,nonatomic) NSArray * neceRespArray;
@property (strong,nonatomic) NSArray * fortDescripcionArray;

@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) IBOutlet UITableView *tabla2;
@end

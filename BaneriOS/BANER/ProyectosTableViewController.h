//
//  ProyectosTableViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"

@interface ProyectosTableViewController : UIViewController<SOAPToolDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITextField *txtDepto;
    IBOutlet UITextField *txtCiudad;
    IBOutlet UIView * ciudadViewContenedor;
    IBOutlet UIPickerView * ciudadPicker;
    NSMutableOrderedSet * ciudadArray;
    NSMutableOrderedSet * deptoArray;
    NSOrderedSet * idDeptoArray;
    NSOrderedSet * idCiudadArray;
    
    NSTimer *tiempo;
}
- (IBAction)cbDepto:(id)sender;
- (IBAction)cbBuscar:(id)sender;
//- (IBAction)Cerrar:(id)sender;
- (IBAction)cbCiudad:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) NSArray * proyNombreArray;
@property (strong, nonatomic) NSArray * proyDescripcionArray;
@property (strong, nonatomic) NSArray * proyFechaIniArray;
@property (strong, nonatomic) NSArray * proyFechaFinArray;
@property (strong, nonatomic) NSArray * proyIdFinArray;

@end

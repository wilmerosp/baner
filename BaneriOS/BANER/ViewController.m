//
//  ViewController.m
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString *deviceType = [UIDevice currentDevice].model;
    NSLog(@"%@",deviceType);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//accion boton salir
- (IBAction)cbSalir:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Salir"message: @"Realmente desea salir de la aplicación?"delegate: nil cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Salir",nil];
    alert.delegate=self;
    [alert show];
}
//validador de salida de app
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        exit(0);
    }
}

@end

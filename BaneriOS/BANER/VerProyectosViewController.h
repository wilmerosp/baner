//
//  VerProyectosViewController.h
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>

@interface VerProyectosViewController : UIViewController
@property (strong,nonatomic) NSString *Nombre;
@property (strong,nonatomic) NSString *Descripcion;
@property (strong,nonatomic) NSString *FechaIni;
@property (strong,nonatomic) NSString *FechaFin;

@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaIni;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaFin;

@property (strong,nonatomic) NSMutableArray *proyNombArray;
@property (strong,nonatomic) NSMutableArray *proyObservArray;
@property (strong,nonatomic) NSMutableArray *proyFechaInicialArray;
@property (strong,nonatomic) NSMutableArray *proyFechaFinalArray;
@end

//
//  FortalezasViewController.h
//  BANER
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import <UIKit/UIKit.h>
#import "SYSoapTool.h"
#import "ViewController.h"

@interface RegFortalezasViewController : UIViewController<SOAPToolDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,UITextFieldDelegate>{
    IBOutlet UIView * deptoViewContenedor;
    IBOutlet UIPickerView * deptoPicker;
    NSMutableOrderedSet * deptoArray;
    NSOrderedSet * DocumentArray;
    NSOrderedSet * FortalezaArray;
    NSOrderedSet * idDeptoArray;
    NSMutableOrderedSet * municipioArray;
    NSMutableOrderedSet *PobladoArray;
    NSOrderedSet *idPobladoArray;
    NSOrderedSet * idMunicipioArray;
    NSOrderedSet * idDocumentArray;
    NSOrderedSet * validarArray;
    NSString * SegNombre;
    NSString * SegApellido;
    IBOutlet UITextField *txtNombre;
    IBOutlet UITextField *txtDireccion;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtNumDocumento;
    IBOutlet UITextField *txtApellido;
    IBOutlet UITextField *txtDocumento;
    IBOutlet UITextField *txtTipoDocumento;
    IBOutlet UITextField *txtDepto;
    IBOutlet UITextField *txtTelefono;
    IBOutlet UITextField *txtMunicipio;
    NSTimer *tiempo;
    IBOutlet UITextField *txtCentroPoblado;
}
@property (nonatomic, strong) NSString *fortaleza;
@property (nonatomic, strong) NSString *observacion;
@property (nonatomic, strong) NSString *documento;

@property (nonatomic, strong) NSString *fortalezaId;

@property (strong, nonatomic) IBOutlet UILabel *lblFortaleza;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;


//- (IBAction)Cerrar:(id)sender;
- (IBAction)cbMunicipio:(id)sender;
- (IBAction)cbDocumento:(id)sender;
- (IBAction)cbDepto:(id)sender;
- (IBAction)cbCentroPoblado:(id)sender;

- (IBAction)cbRegistro:(id)sender;


@end

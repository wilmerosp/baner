//
//  VerZonaViewController.m
//  BANER
//
//  Desarrollado por : 3D APPS S.A.S
//  NIT: 900610482
//  On 28-08-14.
//  Copyright (c) 2014 3DApps. All rights reserved.
//
//  NOTA: se sigue protocolo estandar de programacion en tres capas
//(clases SOAP que manejan la conexion al servidor - Clases de validacion de logica de negocio - Storyboard con las clases de vista)
#import "VerZonaViewController.h"
#import "ZonaViewController.h"

@interface VerZonaViewController ()

@end

@implementation VerZonaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //recibir datos de la Vista Zona
    self.lblEstado.text=self.Estado;
    self.lblObservacion.text=self.Observacion;
    NSLog(@"%@",self.lblEstado.text);
    NSLog(@"%@",self.lblObservacion.text);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


//enviar datos a la vista de resumen
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"volverZona"]) {
        [[segue destinationViewController]setNeceNombreArray:_NecArray];
        [[segue destinationViewController]setFortNombreArray:_FortArray];
        [[segue destinationViewController]setFortDescripcionArray:_descArray];
        [[segue destinationViewController]setNeceObservaArray:_obArray];
        [[segue destinationViewController]setNeceEstadoArray:_estArray];
        [[segue destinationViewController]setFortDescripcionArray:_descArray];
        [[segue destinationViewController]setDepto:_Depto];
        [[segue destinationViewController]setMunic:_Munic];
    }
}

- (IBAction)cbRegresar:(id)sender {
    [self performSegueWithIdentifier:@"volverZona" sender:self];
}
@end
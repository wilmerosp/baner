package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class Projects extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";

    //For Departments WS
    private static final String METHOD_NAME_DEPARTMENTS = "ObtenerListadoDepartamento";
    private static final String SOAP_ACTION_BRING_DEPARTMENTS = "http://tempuri.org/ObtenerListadoDepartamento";

    //For City WS
    private static final String METHOD_NAME_CITIES = "ObtenerListadoMunicipio";
    private static final String SOAP_ACTION_BRING_CITIES  = "http://tempuri.org/ObtenerListadoMunicipio";

    //For Projects WS
    private static final String METHOD_NAME_OBTAIN_PROJECTS = "ObtenerProyecto";
    private static final String SOAP_ACTION_BRING_PROJECTS = "http://tempuri.org/ObtenerProyecto";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;
    private SoapObject resultsRequestSOAPProjects = null;

    //UI control Variables
    private Spinner spnDepartments;
    private Spinner spnCities;

    //For Cities and Departments
    private String[] strDepartments;
    private String[] strDepartmentsCode;
    private String[] strCities;
    private String[] strCitiesCode;

    private int SelectedDepartment;
    private int SelectedCity;

    //For Projects
    private String[] strProjectNames;
    private String[] strProjectDescriptions;
    private String[] strProjectDateBegin;
    private String[] strProjectDateEnd;

    private ListView lstVwProjects;

    //Projects Handler
    private boolean haveProjects;

    //Error handler
    private boolean exc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_projects);

            DownloadDepartment dDepartment = new DownloadDepartment();
            dDepartment.execute();

            lstVwProjects = (ListView) findViewById(R.id.ltVwProjects);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Download Departments
    private class DownloadDepartment extends AsyncTask<Void, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Projects.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused)
        {
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_DEPARTMENTS);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_DEPARTMENTS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();

                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();


                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];

                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject DepartmentsList = (SoapObject) property;
                                //Nombre
                                strDepartments[i + 1] = DepartmentsList.getProperty("Nombre").toString();
                                //Codigo
                                strDepartmentsCode[i + 1] = DepartmentsList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount = +1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];
                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";
                    }
                }
                else exc = true;
            }catch(Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(Projects.this,"Error de Conexion, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Projects.this.finish();
            }
            else{
                exc = false;
                SpinnerDepartments();
            }
        }
    }

    //Download Cities
    private class DownloadCities extends AsyncTask<Integer,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Projects.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params)
        {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_CITIES);

            //Propertyinfo
            PropertyInfo userCityPI = new PropertyInfo();
            //Property Name
            userCityPI.setName("codigoDepartamento");
            //Property Value
            userCityPI.setValue(params[0]);
            //Property Datatype
            userCityPI.setType(int.class);
            //Add property to request
            request.addProperty(userCityPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_CITIES, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                int  intPropertyCount = resultsRequestSOAP.getPropertyCount();

                if (intPropertyCount != 0) {
                    intPropertyCount = intPropertyCount + 1;

                    strCities = new String[intPropertyCount];
                    strCitiesCode = new String[intPropertyCount];

                    strCities[0] = "SELECCIONE";
                    strCitiesCode[0] = "0x";

                    for (int i = 0; i < intPropertyCount -1; i++) {
                        Object property = resultsRequestSOAP.getProperty(i);
                        if (property instanceof SoapObject) {
                            SoapObject CitiesList = (SoapObject) property;
                            //Nombre
                            strCities[i+1] = CitiesList.getProperty("Nombre").toString();
                            //Codigo
                            strCitiesCode[i+1] = CitiesList.getProperty("Codigo").toString();
                        }
                    }
                }
                else {
                    intPropertyCount += 1;
                    strCities = new String[intPropertyCount];
                    strCitiesCode = new String[intPropertyCount];
                    strCities[0] = "SELECCIONE";
                    strCitiesCode[0] = "0x";
                }
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(Projects.this,"Error de Conexion, intente nuevamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Projects.this.finish();
                } else {
                    exc = false;
                    SpinnerCities();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //Spinner for Departments
    public void SpinnerDepartments()
    {
        try {
            //Departments Spinner control
            spnDepartments = (Spinner) findViewById(R.id.spnDepartmentsProjects);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strDepartments);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnDepartments.setAdapter(adapter);

            spnDepartments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SelectedDepartment = spnDepartments.getSelectedItemPosition();
                    if (!strDepartmentsCode[SelectedDepartment].toString().equals("0x")) {
                        DownloadCities cities = new DownloadCities();
                        cities.execute(Integer.valueOf(strDepartmentsCode[SelectedDepartment]));
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Spinner for Cities
    public void  SpinnerCities()
    {
        try {
            //Cities Spinner control
            spnCities = (Spinner) findViewById(R.id.spnCityProjects);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strCities);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnCities.setAdapter(adapter);
            spnCities.setSelection(0);

            SelectedCity = 0000;

            spnCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strCitiesCode[spnCities.getSelectedItemPosition()].toString().equals("0x")) {
                        SelectedCity = Integer.parseInt(strCitiesCode[spnCities.getSelectedItemPosition()]);
                    }
                    else SelectedCity = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void buttonOnClickSearchProjects(View v){

        if(!(SelectedCity == 0000)) {
            DownloadProjects BringProjects = new DownloadProjects();
            BringProjects.execute(SelectedCity);
        }
        else {
            Toast toast = Toast.makeText(Projects.this,"Seleccione un Municipio", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    //AsyncTask Download projects
    private class DownloadProjects extends AsyncTask<Integer,Void,Void>{

        private final ProgressDialog dialog = new ProgressDialog(Projects.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_OBTAIN_PROJECTS);

            //Propertyinfo
            PropertyInfo userCityPI = new PropertyInfo();
            //Property Name
            userCityPI.setName("codigoMunicipio");
            //Property Value
            userCityPI.setValue(params[0]);
            //Property Datatype
            userCityPI.setType(int.class);
            //Add property to request
            request.addProperty(userCityPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_PROJECTS, envelope);
                //Create SoapObject and obtain response
                resultsRequestSOAPProjects = (SoapObject)envelope.getResponse();
                int  intPropertyCount = resultsRequestSOAPProjects.getPropertyCount();

                if(intPropertyCount > 0) {
                    haveProjects = true;
                    strProjectNames = new String[intPropertyCount];
                    strProjectDescriptions = new String[intPropertyCount];
                    strProjectDateBegin = new String[intPropertyCount];
                    strProjectDateEnd = new String[intPropertyCount];

                    for (int i = 0; i < intPropertyCount; i++) {
                        Object property = resultsRequestSOAPProjects.getProperty(i);
                        if (property instanceof SoapObject) {
                            SoapObject CitiesList = (SoapObject) property;
                            //Nombre
                            strProjectNames[i] = CitiesList.getProperty("Nombre").toString();
                            //Codigo
                            strProjectDescriptions[i] = CitiesList.getProperty("Descripcion").toString();
                            //Nombre
                            strProjectDateBegin[i] = CitiesList.getProperty("FechaInicio").toString();
                            //Codigo
                            strProjectDateEnd[i] = CitiesList.getProperty("FechaFin").toString();
                        }
                    }
                }
                else haveProjects = false;
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(Projects.this,"Error de Conexion, intente nuevamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Projects.this.finish();
                } else {
                    if(haveProjects) {
                        exc = false;
                        Toast toast = Toast.makeText(Projects.this,"Seleccione un proyecto para ver la información detallada", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        populateListView();
                    }
                    else{
                        lstVwProjects.setAdapter(null);
                        Toast toast = Toast.makeText(Projects.this,"No hay proyectos en este municipio.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    private void populateListView(){

        ListAdapter listAdapter = new ArrayAdapter<String>(Projects.this, R.layout.perzonalicedlistview, strProjectNames);
        lstVwProjects.setAdapter(listAdapter);

        lstVwProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent mainIntent = new Intent(Projects.this, ProjectDescription.class);
                mainIntent.putExtra("Proyecto", strProjectNames[position].toString());
                mainIntent.putExtra("Descripcion",strProjectDescriptions[position].toString());
                mainIntent.putExtra("Inicio", strProjectDateBegin[position].toString());
                mainIntent.putExtra("Fin", strProjectDateEnd[position].toString());
                Projects.this.startActivity(mainIntent);
            }
        });
    }

    public void buttonOnClickCancelProjects(View v) {
        Projects.this.finish();
    }

}

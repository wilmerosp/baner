package com.agroton.incoder.baner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class MyActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGHT=1000;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_my);

       new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
               Intent mainIntent = new Intent(MyActivity.this, MainMenu.class);
               MyActivity.this.startActivity(mainIntent);
               MyActivity.this.finish();
           }
       }, SPLASH_DISPLAY_LENGHT);

    }

}

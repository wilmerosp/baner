/*
Desarrollado por Santiago Cuervo
Julio 20 de 2014

 */

package com.agroton.incoder.baner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;



public class MainMenu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void buttonOnClickNeed(View v)
    {
        Intent mainIntent = new Intent(MainMenu.this, Needs.class);
        MainMenu.this.startActivity(mainIntent);
    }

    public void buttonOnClickSeeNeeds(View v)
    {
        Intent mainIntent = new Intent(MainMenu.this, SeeNeeds.class);
        MainMenu.this.startActivity(mainIntent);
    }

    public void buttonOnClickStrengths(View v)
    {
        Intent mainIntent = new Intent(MainMenu.this, Strengths.class);
        MainMenu.this.startActivity(mainIntent);
    }

    public void buttonOnClickProjects(View v)
    {
        Intent mainIntent = new Intent(MainMenu.this, Projects.class);
        MainMenu.this.startActivity(mainIntent);
    }

    public void buttonOnClickZone(View v)
    {
        Intent mainIntent = new Intent(MainMenu.this, Zone.class);
        MainMenu.this.startActivity(mainIntent);
    }

    public void buttonOnClickQuit(View v)
    {
        finish();
        System.exit(0);
    }


    //For About screen option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            switch(item.getItemId()){
                case R.id.action_settings:
                    Intent mainIntent = new Intent(MainMenu.this, About.class);
                    MainMenu.this.startActivity(mainIntent);
                    return true;
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

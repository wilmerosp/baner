package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class Strengths extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";
    //To obtain Strenghts WS
    private static final String METHOD_NAME_STRENGTS = "ObtenerListadoFortaleza";
    private static final String SOAP_ACTION_BRING_STRENGHTS = "http://tempuri.org/ObtenerListadoFortaleza";
    //For User Validation
    private static final String METHOD_NAME_USER_VALIDATION = "ValidarUsuario";
    private static final String SOAP_ACTION_VALIDATE_USER = "http://tempuri.org/ValidarUsuario";
    //For Save Strenghts WS
    private static final String METHOD_NAME_SAVE_STRENGHT = "RegistrarFortaleza";
    private static final String SOAP_ACTION_SAVE_STRENGHT = "http://tempuri.org/RegistrarFortaleza";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;
    private SoapPrimitive resultRequestUserRegistrySOAP = null;

    //UI control variables
    private EditText edTxtUserStrengthDescription;
    private EditText edTxtUserDocumentNumber;
    private Spinner spnStrength;
    private String[] strStrength;
    private String[] strCodeStrengths;
    private String strUserDocNumber;
    private String UserValidated;
    private int spnOptionSelected;

    private boolean StrengthRegistered = false;

    //Error handler
    private boolean exc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_strengths);

            edTxtUserStrengthDescription = (EditText)findViewById(R.id.edTxtStrengthDescription);
            edTxtUserDocumentNumber = (EditText)findViewById(R.id.edTxtDocNumberStrengths);
            //AsyncTask class to download Needs
            DownloadStrengths dStrengths = new DownloadStrengths();
            dStrengths.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Download Strengths
    private class DownloadStrengths extends AsyncTask<Void, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Strengths.this);

        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            //android.os.Debug.waitForDebugger();
            //Todo
            //Invoke web method
            //request = new SoapObject(NAMESPACE, METHOD_NAME_STRENGTS);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true; //Service is .net = true

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_STRENGHTS, envelope);

                //create SoapObject and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strStrength = new String[intPropertyCount];
                        strCodeStrengths = new String[intPropertyCount];

                        strStrength[0] = "SELECCIONE";
                        strCodeStrengths[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            //Format the information from the web service
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject needsList = (SoapObject) property;
                                strStrength[i + 1] = needsList.getProperty("Descripcion").toString();
                                strCodeStrengths[i + 1] = needsList.getProperty("IdFortaleza").toString();
                            }
                        }
                    } else {
                        intPropertyCount = intPropertyCount + 1;
                        strStrength = new String[intPropertyCount];
                        strCodeStrengths = new String[intPropertyCount];
                        strStrength[0] = "SELECCIONE";
                        strCodeStrengths[0] = "0x";

                    }
                }else exc = true; //no information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){

            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(Strengths.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Strengths.this.finish();
            }
            else {
                spinnerStrengths();
                exc = false;
            }
        }
    }

    public void spinnerStrengths() {
        //Needs Spinner Control
        spnStrength = (Spinner)findViewById(R.id.spnStrengths);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Strengths.this, R.layout.spinner_item, strStrength);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnStrength.setAdapter(adapter);
        spnStrength.setSelection(0);

        spnStrength.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Todo
                spnOptionSelected = spnStrength.getSelectedItemPosition();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    public void buttonOnClickRegistryStrength(View v){

        if (strCodeStrengths[spnOptionSelected].toString().equals("0x"))
        {
            Toast toast = Toast.makeText(Strengths.this,"Seleccione una fortaleza de la lista", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if (edTxtUserStrengthDescription.getText().length() < 1){
            Toast toast = Toast.makeText(Strengths.this,"Debe registrar una descripción de la fortaleza", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if(edTxtUserDocumentNumber.getText().length() < 5){
            Toast toast = Toast.makeText(Strengths.this,"Digite un documento valido", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else {
            strUserDocNumber = edTxtUserDocumentNumber.getText().toString();
            ValidateUser validateUser = new ValidateUser();
            validateUser.execute(strUserDocNumber);
        }
    }

    //Validate User
    private class ValidateUser extends AsyncTask<String, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Strengths.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Validando documento");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            //android.os.Debug.waitForDebugger();
            //Todo
            //Invoke web method
            try{
                request = new SoapObject(NAMESPACE, METHOD_NAME_USER_VALIDATION);
                request.addProperty("numeroCedula", params[0]);

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true; //Service is .net = true

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_VALIDATE_USER, envelope);

                //create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive)envelope.getResponse();
                UserValidated = resultRequestUserRegistrySOAP.toString();

            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            //Debug.waitForDebugger();
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(Strengths.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Strengths.this.finish();
            }
            else {
                exc = false;
                if(UserValidated.equals("OK")){
                    RegistryUserStrength registryUserStrength = new RegistryUserStrength();
                    registryUserStrength.execute();
                }
                else if (UserValidated.equals("NO")){
                    Intent mainIntent = new Intent(Strengths.this, SaveStrength.class);
                    mainIntent.putExtra("Fortaleza", strStrength[spnOptionSelected].toString());
                    mainIntent.putExtra("Codigo", strCodeStrengths[spnOptionSelected].toString());
                    mainIntent.putExtra("Descripcion",edTxtUserStrengthDescription.getText().toString());
                    mainIntent.putExtra("DocumentNumber",edTxtUserDocumentNumber.getText().toString());
                    Strengths.this.startActivity(mainIntent);
                    Strengths.this.finish();
                }
                else {
                    Toast toast = Toast.makeText(Strengths.this,"No se tiene acceso al servidor, verifique su conexión", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }

    //Registry User Strength
    private class RegistryUserStrength extends AsyncTask<Void,Void,Void> {

        private final ProgressDialog dialog = new ProgressDialog(Strengths.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Almacenando Fortaleza");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try {

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE_STRENGHT);

                request.addProperty("codigoFortaleza", strCodeStrengths[spnOptionSelected].toString());
                request.addProperty("observacion", edTxtUserStrengthDescription.getText().toString());
                request.addProperty("cedula", strUserDocNumber);

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_SAVE_STRENGHT, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive) envelope.getResponse();
                StrengthRegistered = Boolean.parseBoolean(String.valueOf(resultRequestUserRegistrySOAP));

            } catch (Exception e) {
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(Strengths.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Strengths.this.finish();
            } else {
                exc = false;
                if(StrengthRegistered){
                    Toast toast = Toast.makeText(Strengths.this,"Fortaleza Almacenada correctamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Strengths.this.finish();
                }
                else{
                    Toast toast = Toast.makeText(Strengths.this,"No ha sido posible registrar la fortaleza, confirme los datos y su conexión a internet", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Strengths.this.finish();
                }
            }
        }
    }

    public void buttonOnClickCancelStrengths(View v)
    {
        Strengths.this.finish();
    }

}

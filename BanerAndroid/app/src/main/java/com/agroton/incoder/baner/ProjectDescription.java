package com.agroton.incoder.baner;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class ProjectDescription extends Activity {

    private TextView txtVwProjectName;
    private TextView txtVwProjectDescription;
    private TextView txtVwProjectBegin;
    private TextView txtVwProjectEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_description);

        txtVwProjectName = (TextView)findViewById(R.id.txtVwProjectName);
        txtVwProjectDescription = (TextView)findViewById(R.id.txtVwProjectDescription);
        txtVwProjectBegin = (TextView)findViewById(R.id.txtVwProjectBegin);
        txtVwProjectEnd = (TextView)findViewById(R.id.txtVwProjectEnd);

        try{
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                txtVwProjectName.setText(extras.getString("Proyecto"));
                txtVwProjectDescription.setText(extras.getString("Descripcion"));
                txtVwProjectBegin.setText(extras.getString("Inicio"));
                txtVwProjectEnd.setText(extras.getString("Fin"));
            }
            else{
                Toast toast = Toast.makeText(ProjectDescription.this,"No se recibió información, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    public void buttonOnClickCancelProjectDescription(View v){
        ProjectDescription.this.finish();
    }

}

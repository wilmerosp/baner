package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class SaveStrength extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";

    //For Document Types WS
    private static final String METHOD_NAME_DOC_TYPES = "ObtenerTiposDocumento";
    private static final String SOAP_ACTION_BRING_DOC_TYPES = "http://tempuri.org/ObtenerTiposDocumento";

    //For Departments WS
    private static final String METHOD_NAME_DEPARTMENTS = "ObtenerListadoDepartamento";
    private static final String SOAP_ACTION_BRING_DEPARTMENTS = "http://tempuri.org/ObtenerListadoDepartamento";

    //For City WS
    private static final String METHOD_NAME_CITIES = "ObtenerListadoMunicipio";
    private static final String SOAP_ACTION_BRING_CITIES  = "http://tempuri.org/ObtenerListadoMunicipio";

    //For Populated Centers WS
    private static final String METHOD_NAME_POPCENTERS = "ObtenerListadoCentroPoblado";
    private static final String SOAP_ACTION_BRING_POPCENTERS  = "http://tempuri.org/ObtenerListadoCentroPoblado";

    //For Registry User WS
    private static final String METHOD_NAME_REGISTRY_USER = "RegistrarUsuario";
    private static final String SOAP_ACTION_REGISTRY_USER = "http://tempuri.org/RegistrarUsuario";

    //For Save Strenghts WS
    private static final String METHOD_NAME_SAVE_STRENGHT = "RegistrarFortaleza";
    private static final String SOAP_ACTION_SAVE_STRENGHT = "http://tempuri.org/RegistrarFortaleza";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;
    private SoapPrimitive resultRequestUserRegistrySOAP = null;

    //UI control Variables
    private Spinner spnDocType;
    private Spinner spnDepartments;
    private Spinner spnCities;
    private Spinner spnPopCenters;
    private TextView ChoosenStrength;
    private EditText UserNames;
    private EditText UserLastNames;
    private EditText UserDocumentNumber;
    private EditText UserEMail;
    private EditText UserAdress;
    private EditText UserTelephone;

    private String[] strDocTypes;
    private String[] strDocTypesCode;
    private String[] strDepartments;
    private String[] strDepartmentsCode;
    private String[] strCities;
    private String[] strCitiesCode;
    private String[] strPopCenter;
    private String[] strPopCenterCodes;
    private String[] UserName;
    private String[] UserLastName;

    private String CodeStrength;
    private String SrengthDescription;
    private String UserDescription;
    private String Latitude = "0";
    private String Longitude = "0";
    private int SelectedDepartment;
    private int SelectedCity;
    private int SelectedCenter;
    private int UserDocType;
    private boolean UserRegistered = false;
    private boolean StrengthRegistered = false;

    //Error handler
    boolean exc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_strength);

        ChoosenStrength = (TextView)findViewById(R.id.txtVwChoosenStrength);
        UserNames = (EditText)findViewById(R.id.edTxtNameStrengths);
        UserLastNames = (EditText)findViewById(R.id.edTxtLastNameStrengths);
        UserDocumentNumber = (EditText)findViewById(R.id.edTxtDocNumberStrengths);
        UserEMail = (EditText)findViewById(R.id.edTxtMailstrengths);
        UserAdress = (EditText)findViewById(R.id.edTxtAdressStrengths);
        UserTelephone = (EditText)findViewById(R.id.edTxtTelephoneStrenghts);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {

                CodeStrength = extras.getString("Codigo");
                SrengthDescription = extras.getString("Fortaleza");
                UserDescription = extras.getString("Descripcion");
                UserDocumentNumber.setText(extras.getString("DocumentNumber"));

                ChoosenStrength.setText(SrengthDescription);

                DownloadDoctype dDoctype = new DownloadDoctype();
                DownloadDepartment dDepartment = new DownloadDepartment();

                dDoctype.execute();
                dDepartment.execute();
            } else {
                Toast toast = Toast.makeText(SaveStrength.this,"No se recibio información, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveStrength.this.finish();
            }

            DownloadDoctype dDoctype = new DownloadDoctype();
            DownloadDepartment dDepartment = new DownloadDepartment();

            dDoctype.execute();
            dDepartment.execute();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Download Document types
    private class DownloadDoctype extends AsyncTask<Void, Void, Void>
    {
        //private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        /*
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando información");
            this.dialog.show();
        }*/

        @Override
        protected Void doInBackground(Void... unused)
        {
            //Todo
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_DOC_TYPES);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_DOC_TYPES, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP= (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strDocTypes = new String[intPropertyCount];
                        strDocTypesCode = new String[intPropertyCount];

                        strDocTypes[0] = "SELECCIONE";
                        strDocTypesCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject DocTypeList = (SoapObject) property;
                                //Descripcion
                                strDocTypes[i + 1] = DocTypeList.getProperty("Descripcion").toString();
                                //Id
                                strDocTypesCode[i + 1] = DocTypeList.getProperty("Id").toString();
                            }
                        }
                    } else {
                        intPropertyCount = intPropertyCount + 1;
                        strDocTypes = new String[intPropertyCount];
                        strDocTypesCode = new String[intPropertyCount];
                        strDocTypes[0] = "SELECCIONE";
                        strDocTypesCode[0] = "0x";
                    }
                }
                else exc = true;//No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            //Debug.waitForDebugger();
            //if(this.dialog.isShowing())
            //{
            //    this.dialog.dismiss();
            //}
            if(exc){
                Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexion, intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveStrength.this.finish();
            }
            else{
                exc = false;
                SpinnerDocType();
            }
        }
    }

    //Download Departments
    private class DownloadDepartment extends AsyncTask<Void, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveStrength.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused)
        {
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_DEPARTMENTS);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_DEPARTMENTS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];
                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject DepartmentsList = (SoapObject) property;
                                //Nombre
                                strDepartments[i + 1] = DepartmentsList.getProperty("Nombre").toString();
                                //Codigo
                                strDepartmentsCode[i + 1] = DepartmentsList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount = +1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];
                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";
                    }
                }
                else exc = true;//No information from WS side
            }catch(Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexion, intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveStrength.this.finish();
            }
            else{
                exc = false;
                SpinnerDepartments();
            }
        }
    }

    //Download Cities
    private class DownloadCities extends AsyncTask<Integer,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveStrength.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params)
        {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_CITIES);

            //Propertyinfo
            PropertyInfo userCityPI = new PropertyInfo();
            //Property Name
            userCityPI.setName("codigoDepartamento");
            //Property Value
            userCityPI.setValue(params[0]);
            //Property Datatype
            userCityPI.setType(int.class);
            //Add property to request
            request.addProperty(userCityPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_CITIES, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if (resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject CitiesList = (SoapObject) property;
                                //Nombre
                                strCities[i + 1] = CitiesList.getProperty("Nombre").toString();
                                //Codigo
                                strCitiesCode[i + 1] = CitiesList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount += 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";
                    }
                }
                else exc = true;//No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexion, intente de nuevo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveStrength.this.finish();
                } else {
                    exc = false;
                    SpinnerCities();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    //Download Population Centers
    private class DownloadPopulationCenters extends AsyncTask<Integer,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveStrength.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params)
        {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_POPCENTERS);

            //Propertyinfo
            PropertyInfo userPopCenterPI = new PropertyInfo();
            //Property Name
            userPopCenterPI.setName("codigoMunicipio");
            //Property Value
            userPopCenterPI.setValue(params[0]);
            //Property Datatype
            userPopCenterPI.setType(int.class);
            //Add property to request
            request.addProperty(userPopCenterPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_POPCENTERS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strPopCenter = new String[intPropertyCount];
                        strPopCenterCodes = new String[intPropertyCount];
                        strPopCenter[0] = "SELECCIONE";
                        strPopCenterCodes[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject PopCenterList = (SoapObject) property;
                                //Nombre
                                strPopCenter[i + 1] = PopCenterList.getProperty("NombreCentroPoblado").toString();
                                //Codigo
                                strPopCenterCodes[i + 1] = PopCenterList.getProperty("Id").toString();
                            }
                        }
                    } else {
                        intPropertyCount += 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";
                    }
                }
                else exc = true;// No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveStrength.this.finish();
                } else {
                    exc = false;
                    SpinnerPopCenters();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //Spinner for Document types
    public void SpinnerDocType()
    {
        try {
            //Document type Spinner control
            spnDocType = (Spinner) findViewById(R.id.spnDocTypeSaveStrengths);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strDocTypes);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnDocType.setAdapter(adapter);
            spnDocType.setSelection(0);

            UserDocType = 0000;
            spnDocType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strDocTypesCode[spnDocType.getSelectedItemPosition()].toString().equals("0x")) {
                        UserDocType = Integer.parseInt(strDocTypesCode[spnDocType.getSelectedItemPosition()]);
                    }
                    else UserDocType = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Spinner for Departments
    public void SpinnerDepartments()
    {
        try {
            //Departments Spinner control
            spnDepartments = (Spinner) findViewById(R.id.spnDepartmentSaveStrengths);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strDepartments);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnDepartments.setAdapter(adapter);
            spnDepartments.setSelection(0);

            spnDepartments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SelectedDepartment = spnDepartments.getSelectedItemPosition();
                    if (!strDepartmentsCode[SelectedDepartment].toString().equals("0x")) {
                        DownloadCities cities = new DownloadCities();
                        cities.execute(Integer.valueOf(strDepartmentsCode[SelectedDepartment]));
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Spinner for Cities
    public void  SpinnerCities()
    {
        try {
            //Cities Spinner control
            spnCities = (Spinner) findViewById(R.id.spnCitieSaveStrength);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strCities);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnCities.setAdapter(adapter);
            spnCities.setSelection(0);

            SelectedCity = 0000;
            spnCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strCitiesCode[spnCities.getSelectedItemPosition()].toString().equals("0x")) {
                        SelectedCity = Integer.parseInt(strCitiesCode[spnCities.getSelectedItemPosition()]);
                        DownloadPopulationCenters downloadPopulationCenters = new DownloadPopulationCenters();
                        downloadPopulationCenters.execute(SelectedCity);
                    }
                    else SelectedCity = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Spinner for Population Centers
    public void  SpinnerPopCenters()
    {
        try {
            //Cities Spinner control
            spnPopCenters = (Spinner) findViewById(R.id.spnPobCenterSaveStrength);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strPopCenter);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnPopCenters.setAdapter(adapter);
            spnPopCenters.setSelection(0);

            SelectedCenter = 0000;
            spnPopCenters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strPopCenterCodes[spnPopCenters.getSelectedItemPosition()].toString().equals("0x")) {
                        SelectedCenter = Integer.parseInt(strPopCenterCodes[spnPopCenters.getSelectedItemPosition()]);
                    }
                    else SelectedCenter = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void buttonOnClickSaveStrengths(View v){
        try {
            if (UserNames.getText().length() < 3){
                Toast toast = Toast.makeText(SaveStrength.this,"DIGITE UN NOMBRE VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserLastNames.getText().length() < 3){
                Toast toast = Toast.makeText(SaveStrength.this,"DIGITE UN APELLIDO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserDocType == 0000){
                Toast toast = Toast.makeText(SaveStrength.this,"SELECCION UN TIPO DE DOCUMENTO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserDocumentNumber.getText().length() < 5){
                Toast toast = Toast.makeText(SaveStrength.this,"DIGITE UN DOCUMENTO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserTelephone.getText().length() < 5){
                Toast toast = Toast.makeText(SaveStrength.this,"DIGITE UN NUMERO DE TELEFONO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (SelectedCity == 0000)
            {
                Toast toast = Toast.makeText(SaveStrength.this,"SELECCIONE UN MUNICIPIO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if(SelectedCenter == 0000){
                Toast toast = Toast.makeText(SaveStrength.this,"SELECCIONE UN CENTRO POBLADO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserAdress.getText().length() < 4) {
                Toast toast = Toast.makeText(SaveStrength.this,"DIGITE UNA DIRECCIÓN VALIDA", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else {
                RegistryUser CallUserRegistry = new RegistryUser();
                CallUserRegistry.execute();
            }
        }catch (Exception e){ e.printStackTrace();}
    }

    //Registry User
    private class RegistryUser extends AsyncTask<Void,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveStrength.this);

        @Override
        protected void onPreExecute()
        {
            this.dialog.setMessage("Almacenando Información de Usuario");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try{

                //divide the Names of the user
                String temp = UserNames.getText().toString();
                UserName = temp.split(" ");
                //divide the last names of the user
                String temp2 = UserLastNames.getText().toString();
                UserLastName = temp2.split(" ");

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_REGISTRY_USER);

                request.addProperty("cedula", UserDocumentNumber.getText().toString());
                request.addProperty("nombre1", UserName[0]);
                if (UserName.length == 1) {
                    request.addProperty("nombre2", " ");
                }
                else{
                    request.addProperty("nombre2", UserName[1]);
                }
                request.addProperty("apellido1", UserLastName[0]);
                if(UserLastName.length == 1){
                    request.addProperty("apellido2", " ");
                }
                else {
                    request.addProperty("apellido2", UserLastName[1]);
                }
                request.addProperty("telefono", UserTelephone.getText().toString());
                request.addProperty("email", UserEMail.getText().toString());
                request.addProperty("direccion", UserAdress.getText().toString());
                request.addProperty("idTipoDocumento",UserDocType);
                request.addProperty("idMunicipio", SelectedCity);
                request.addProperty("idCentroPoblado", Integer.valueOf(SelectedCenter));

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_REGISTRY_USER, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive)envelope.getResponse();
                UserRegistered = Boolean.parseBoolean(String.valueOf(resultRequestUserRegistrySOAP));

            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result)
        {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexion, intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveStrength.this.finish();
            } else {
                exc = false;
                if(UserRegistered){
                    RegistryUserStrength registryUserStrength = new RegistryUserStrength();
                    registryUserStrength.execute();
                }
                else {
                    Toast toast = Toast.makeText(SaveStrength.this,"Error de Registro de Usuario, intente de nuevo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }

    //Registry User Strength
    private class RegistryUserStrength extends AsyncTask<Void,Void,Void> {

        private final ProgressDialog dialog = new ProgressDialog(SaveStrength.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Almacenando Fortaleza");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try {

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE_STRENGHT);

                request.addProperty("codigoFortaleza", CodeStrength);
                request.addProperty("observacion", UserDescription);
                request.addProperty("cedula", UserDocumentNumber.getText().toString());

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_SAVE_STRENGHT, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive) envelope.getResponse();
                StrengthRegistered = Boolean.parseBoolean(String.valueOf(resultRequestUserRegistrySOAP));

            } catch (Exception e) {
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(SaveStrength.this,"Error de Conexion, intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveStrength.this.finish();
            } else {
                exc = false;
                if (StrengthRegistered) {
                    Toast toast = Toast.makeText(SaveStrength.this,"Fortaleza Almacenada correctamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveStrength.this.finish();
                }
                else {
                    Toast toast = Toast.makeText(SaveStrength.this,"Error al almacenar fortaleza, intente de nuevo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveStrength.this.finish();
                }
            }
        }
    }

    public void buttonOnClickCancelSaveStrengths(View v)
    {
        SaveStrength.this.finish();
    }

}



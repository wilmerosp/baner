package com.agroton.incoder.baner;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class ZoneDescription extends Activity {

    private TextView txtVwProjectName;
    private TextView txtVwProjectDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_description);

        txtVwProjectName = (TextView)findViewById(R.id.txtVwProyectZone);
        txtVwProjectDescription = (TextView)findViewById(R.id.txtVwDescriptionZone);

        try{
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                txtVwProjectName.setText(extras.getString("Proyecto"));
                txtVwProjectDescription.setText(extras.getString("Descripcion"));
            }
            else{
                Toast toast = Toast.makeText(ZoneDescription.this,"No se recibió información, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void buttonOnClickCancelZoneDescription(View v)
    {
        ZoneDescription.this.finish();
    }
}

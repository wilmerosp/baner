package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class SaveNeed extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";

    //For Document Types WS
    private static final String METHOD_NAME_DOC_TYPES = "ObtenerTiposDocumento";
    private static final String SOAP_ACTION_BRING_DOC_TYPES = "http://tempuri.org/ObtenerTiposDocumento";

    //For Departments WS
    private static final String METHOD_NAME_DEPARTMENTS = "ObtenerListadoDepartamento";
    private static final String SOAP_ACTION_BRING_DEPARTMENTS = "http://tempuri.org/ObtenerListadoDepartamento";

    //For City WS
    private static final String METHOD_NAME_CITIES = "ObtenerListadoMunicipio";
    private static final String SOAP_ACTION_BRING_CITIES  = "http://tempuri.org/ObtenerListadoMunicipio";

    //For Populated Centers WS
    private static final String METHOD_NAME_POPCENTERS = "ObtenerListadoCentroPoblado";
    private static final String SOAP_ACTION_BRING_POPCENTERS  = "http://tempuri.org/ObtenerListadoCentroPoblado";

    //For Registry User WS
    private static final String METHOD_NAME_REGISTRY_USER = "RegistrarUsuario";
    private static final String SOAP_ACTION_REGISTRY_USER = "http://tempuri.org/RegistrarUsuario";

    //For Save Needs WS
    private static final String METHOD_NAME_SAVE_NEED = "RegistrarNecesidad";
    private static final String SOAP_ACTION_SAVE_NEED = "http://tempuri.org/RegistrarNecesidad";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;
    private SoapPrimitive resultRequestUserRegistrySOAP = null;
    private SoapPrimitive resultRequestSaveNeedSOAP = null;

    //UI control variables
    private Spinner spnDocType;
    private Spinner spnDepartments;
    private Spinner spnCities;
    private Spinner spnPopCenters;
    private TextView ChoosenNeed;
    private EditText UserNames;
    private EditText UserLastNames;
    private EditText UserDocumentNumber;
    private EditText UserEMail;
    private EditText UserAdress;
    private EditText UserTelephone;

    private String[] strDocTypes;
    private String[] strDocTypesCode;
    private String[] strDepartments;
    private String[] strDepartmentsCode;
    private String[] strCities;
    private String[] strCitiesCode;
    private String[] strPopCenter;
    private String[] strPopCenterCodes;

    private String CodeNeed;
    private String NeedDescription;
    private String UserDescription;
    private String UserDocument;
    private String Latitude = "0";
    private String Longitude = "0";
    private String[] UserName;
    private String[] UserLastName;
    private int SelectedDepartment;
    private int SelectedCity;
    private int UserDocType;
    private int SelectedCenter;
    private boolean UserRegistered = false;
    private boolean NeedRegistered = false;

    //Error handler
    boolean exc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_need);

        ChoosenNeed = (TextView)findViewById(R.id.txtVwChoosenNeed);
        UserNames = (EditText)findViewById(R.id.edTxtNameSaveNeed);
        UserLastNames = (EditText)findViewById(R.id.edTxtLastNameSaveNeed);
        UserDocumentNumber = (EditText)findViewById(R.id.edTxtDocNumberSaveNeed);
        UserEMail = (EditText)findViewById(R.id.edTxtMailSaveNeed);
        UserAdress = (EditText)findViewById(R.id.edTxtAdressSaveNeed);
        UserTelephone = (EditText)findViewById(R.id.edTxtTelephoneSaveNeed);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {

                CodeNeed = extras.getString("Codigo");
                NeedDescription = extras.getString("Necesidad");
                UserDescription = extras.getString("Descripcion");
                UserDocument = extras.getString("Cedula");

                ChoosenNeed.setText(NeedDescription);
                UserDocumentNumber.setText(UserDocument);

                DownloadDoctype dDoctype = new DownloadDoctype();
                DownloadDepartment dDepartment = new DownloadDepartment();

                dDoctype.execute();
                dDepartment.execute();
            } else {
                Toast toast = Toast.makeText(SaveNeed.this,"No se recibio información, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveNeed.this.finish();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    //Download Document types
    private class DownloadDoctype extends AsyncTask<Void, Void, Void>
    {
        //private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        /*
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando información");
            this.dialog.show();
        }*/

        @Override
        protected Void doInBackground(Void... unused)
        {
            //Todo
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_DOC_TYPES);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_DOC_TYPES, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP= (SoapObject)envelope.getResponse();

                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strDocTypes = new String[intPropertyCount];
                        strDocTypesCode = new String[intPropertyCount];

                        strDocTypes[0] = "SELECCIONE";
                        strDocTypesCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject DocTypeList = (SoapObject) property;
                                //Descripcion
                                strDocTypes[i + 1] = DocTypeList.getProperty("Descripcion").toString();
                                //Id
                                strDocTypesCode[i + 1] = DocTypeList.getProperty("Id").toString();
                            }
                        }
                    } else {
                        intPropertyCount = intPropertyCount + 1;
                        strDocTypes = new String[intPropertyCount];
                        strDocTypesCode = new String[intPropertyCount];
                        strDocTypes[0] = "SELECCIONE";
                        strDocTypesCode[0] = "0x";
                    }
                }
                else exc = true;//No information for WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            //Debug.waitForDebugger();
            //if(this.dialog.isShowing())
            //{
            //    this.dialog.dismiss();
            //}
            if(exc){
                Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveNeed.this.finish();
            }
            else{
                exc = false;
                SpinnerDocType();
            }
        }
    }

    //Download Departments
    private class DownloadDepartment extends AsyncTask<Void, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused)
        {
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_DEPARTMENTS);
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_DEPARTMENTS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];

                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject DepartmentsList = (SoapObject) property;
                                //Nombre
                                strDepartments[i + 1] = DepartmentsList.getProperty("Nombre").toString();
                                //Codigo
                                strDepartmentsCode[i + 1] = DepartmentsList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount = +1;
                        strDepartments = new String[intPropertyCount];
                        strDepartmentsCode = new String[intPropertyCount];
                        strDepartments[0] = "SELECCIONE";
                        strDepartmentsCode[0] = "0x";
                    }
                }
                else exc = true;//No information from WS side
            }catch(Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveNeed.this.finish();
            }
            else{
                exc = false;
                SpinnerDepartments();
            }
        }
    }

    //Download Cities
    private class DownloadCities extends AsyncTask<Integer,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params)
        {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_CITIES);

            //Propertyinfo
            PropertyInfo userCityPI = new PropertyInfo();
            //Property Name
            userCityPI.setName("codigoDepartamento");
            //Property Value
            userCityPI.setValue(params[0]);
            //Property Datatype
            userCityPI.setType(int.class);
            //Add property to request
            request.addProperty(userCityPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_CITIES, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject CitiesList = (SoapObject) property;
                                //Nombre
                                strCities[i + 1] = CitiesList.getProperty("Nombre").toString();
                                //Codigo
                                strCitiesCode[i + 1] = CitiesList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount += 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";
                    }
                }
                else exc = true;// No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveNeed.this.finish();
                } else {
                    exc = false;
                    SpinnerCities();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    //Download Population Centers
    private class DownloadPopulationCenters extends AsyncTask<Integer,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params)
        {
            //Debug.waitForDebugger();
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_POPCENTERS);

            //Propertyinfo
            PropertyInfo userPopCenterPI = new PropertyInfo();
            //Property Name
            userPopCenterPI.setName("codigoMunicipio");
            //Property Value
            userPopCenterPI.setValue(params[0]);
            //Property Datatype
            userPopCenterPI.setType(int.class);
            //Add property to request
            request.addProperty(userPopCenterPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_POPCENTERS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();
                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strPopCenter = new String[intPropertyCount];
                        strPopCenterCodes = new String[intPropertyCount];
                        strPopCenter[0] = "SELECCIONE";
                        strPopCenterCodes[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject PopCenterList = (SoapObject) property;
                                //Nombre
                                strPopCenter[i + 1] = PopCenterList.getProperty("NombreCentroPoblado").toString();
                                //Codigo
                                strPopCenterCodes[i + 1] = PopCenterList.getProperty("Id").toString();
                            }
                        }
                    } else {
                        intPropertyCount += 1;
                        strCities = new String[intPropertyCount];
                        strCitiesCode = new String[intPropertyCount];
                        strCities[0] = "SELECCIONE";
                        strCitiesCode[0] = "0x";
                    }
                }
                else exc = true;// No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            try {
                if (this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
                if (exc) {
                    Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveNeed.this.finish();
                } else {
                    exc = false;
                    SpinnerPopCenters();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //Spinner for Document types
    public void SpinnerDocType()
    {
        try {
            //Document type Spinner control
            spnDocType = (Spinner) findViewById(R.id.spnDocTypeSaveNeed);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strDocTypes);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnDocType.setAdapter(adapter);
            spnDocType.setSelection(0);

            UserDocType = 0000;
            spnDocType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strDocTypesCode[spnDocType.getSelectedItemPosition()].toString().equals("0x")) {
                        UserDocType = Integer.parseInt(strDocTypesCode[spnDocType.getSelectedItemPosition()]);
                    }
                    else UserDocType = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Spinner for Departments
    public void SpinnerDepartments()
    {
        try {
            //Departments Spinner control
            spnDepartments = (Spinner) findViewById(R.id.spnDepartmentSaveNeed);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strDepartments);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnDepartments.setAdapter(adapter);
            spnDepartments.setSelection(0);

            spnDepartments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SelectedDepartment = spnDepartments.getSelectedItemPosition();
                    if (!strDepartmentsCode[SelectedDepartment].toString().equals("0x")) {
                        DownloadCities cities = new DownloadCities();
                        cities.execute(Integer.valueOf(strDepartmentsCode[SelectedDepartment]));
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Spinner for Cities
    public void  SpinnerCities()
    {
        try {
            //Cities Spinner control
            spnCities = (Spinner) findViewById(R.id.spnCitieSaveNeed);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strCities);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnCities.setAdapter(adapter);
            spnCities.setSelection(0);

            SelectedCity = 0000;
            spnCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strCitiesCode[spnCities.getSelectedItemPosition()].toString().equals("0x")) {
                        SelectedCity = Integer.parseInt(strCitiesCode[spnCities.getSelectedItemPosition()]);
                        DownloadPopulationCenters downloadPopulationCenters = new DownloadPopulationCenters();
                        downloadPopulationCenters.execute(SelectedCity);
                    }
                    else SelectedCity = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Spinner for Population Centers
    public void  SpinnerPopCenters()
    {
        try {
            //Cities Spinner control
            spnPopCenters = (Spinner) findViewById(R.id.spnPobCenterSaveNeed);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, strPopCenter);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnPopCenters.setAdapter(adapter);
            spnPopCenters.setSelection(0);

            SelectedCenter = 0000;
            spnPopCenters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!strPopCenterCodes[spnPopCenters.getSelectedItemPosition()].toString().equals("0x")) {
                        SelectedCenter = Integer.parseInt(strPopCenterCodes[spnPopCenters.getSelectedItemPosition()]);
                    }
                    else SelectedCenter = 0000;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void buttonOnClickSaveNeed(View v)
    {
        try {
            if (UserNames.getText().length() < 3) {
                Toast toast = Toast.makeText(SaveNeed.this,"DIGITE UN NOMBRE VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if ( UserLastNames.getText().length() < 3){
                Toast toast = Toast.makeText(SaveNeed.this,"DIGITE UN APELLIDO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserDocType == 0000){
                Toast toast = Toast.makeText(SaveNeed.this,"SELECCION UN TIPO DE DOCUMENTO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserDocumentNumber.getText().length() < 5){
                Toast toast = Toast.makeText(SaveNeed.this,"DIGITE UN DOCUMENTO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserTelephone.getText().length() < 5){
                Toast toast = Toast.makeText(SaveNeed.this,"DIGITE UN NUMERO DE TELEFONO VALIDO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (SelectedCity == 0000)
            {
                Toast toast = Toast.makeText(SaveNeed.this,"SELECCIONE UN MUNICIPIO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if(SelectedCenter == 0000){
                Toast toast = Toast.makeText(SaveNeed.this,"SELECCIONE UN CENTRO POBLADO", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else if (UserAdress.getText().length() < 4){
                Toast toast = Toast.makeText(SaveNeed.this,"DIGITE UNA DIRECCIÓN VALIDA", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            else {
                RegistryUser CallUserRegistry = new RegistryUser();
                CallUserRegistry.execute();
            }
        }catch (Exception e){ e.printStackTrace();}
    }

    //Registry User
    private class RegistryUser extends AsyncTask<Void,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);
        @Override
        protected void onPreExecute()
        {
            this.dialog.setMessage("Almacenando Información de Usuario");
            this.dialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try{

                //divide the Names of the user
                String temp = UserNames.getText().toString();
                UserName = temp.split(" ");
                //divide the last names of the user
                String temp2 = UserLastNames.getText().toString();
                UserLastName = temp2.split(" ");

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_REGISTRY_USER);

                request.addProperty("cedula", UserDocumentNumber.getText().toString());
                request.addProperty("nombre1", UserName[0]);
                if (UserName.length == 1) {
                    request.addProperty("nombre2", " ");
                }
                else{
                    request.addProperty("nombre2", UserName[1]);
                }
                request.addProperty("apellido1", UserLastName[0]);
                if(UserLastName.length == 1){
                    request.addProperty("apellido2", " ");
                }
                else {
                    request.addProperty("apellido2", UserLastName[1]);
                }
                request.addProperty("telefono", UserTelephone.getText().toString());
                if (UserEMail.getText().toString().length() != 0) {
                    request.addProperty("email", UserEMail.getText().toString());
                }
                else {
                    request.addProperty("email", " ");
                }
                request.addProperty("direccion", UserAdress.getText().toString());
                request.addProperty("idTipoDocumento",UserDocType);
                request.addProperty("idMunicipio", SelectedCity);
                request.addProperty("idCentroPoblado", Integer.valueOf(SelectedCenter));

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_REGISTRY_USER, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive)envelope.getResponse();
                UserRegistered = Boolean.parseBoolean(String.valueOf(resultRequestUserRegistrySOAP));

            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result)
        {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                SaveNeed.this.finish();
            } else {
                exc = false;
                if(UserRegistered){
                    RegistryUserNeed registryUserNeed = new RegistryUserNeed();
                    registryUserNeed.execute();
                }
                else {
                    //User registry return false
                    Toast toast = Toast.makeText(SaveNeed.this,"No ha sido posible registrar la necesidad, confirme los datos y su conexión a internet", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }
        }
    }


    //Registry User Need
    private class RegistryUserNeed extends AsyncTask<Void,Void,Void> {

        private final ProgressDialog dialog = new ProgressDialog(SaveNeed.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Almacenando Necesidad");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try {

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE_NEED);

                request.addProperty("codigoNecesidad", CodeNeed);
                request.addProperty("observacion", UserDescription);
                request.addProperty("latitud", Latitude);
                request.addProperty("longitud", Longitude);
                request.addProperty("cedula", UserDocumentNumber.getText().toString());

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_SAVE_NEED, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestSaveNeedSOAP = (SoapPrimitive) envelope.getResponse();
                NeedRegistered = Boolean.parseBoolean(String.valueOf(resultRequestSaveNeedSOAP));

            } catch (Exception e) {
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(SaveNeed.this,"Error de Conexión, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                SaveNeed.this.finish();
            } else {
                exc = false;
                if(NeedRegistered){
                    Toast toast = Toast.makeText(SaveNeed.this,"Los datos han sido registrados exitosamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveNeed.this.finish();
                }
                else{
                    Toast toast = Toast.makeText(SaveNeed.this,"No ha sido posible registrar la necesidad, confirme los datos y su conexión a internet", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    SaveNeed.this.finish();
                }
            }
        }
    }


    public void buttonOnClickCancelSaveNeed(View v)
    {
        SaveNeed.this.finish();
    }
}

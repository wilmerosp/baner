package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;



public class Needs extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";
    //For Needs WS
    private static final String METHOD_NAME_NEEDS = "ObtenerListadoNecesidad";
    private static final String SOAP_ACTION_BRING_NEEDS = "http://tempuri.org/ObtenerListadoNecesidad";
    //For User Validation
    private static final String METHOD_NAME_USER_VALIDATION = "ValidarUsuario";
    private static final String SOAP_ACTION_VALIDATE_USER = "http://tempuri.org/ValidarUsuario";
    //For Save Needs WS
    private static final String METHOD_NAME_SAVE_NEED = "RegistrarNecesidad";
    private static final String SOAP_ACTION_SAVE_NEED = "http://tempuri.org/RegistrarNecesidad";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;
    private SoapPrimitive resultRequestUserRegistrySOAP = null;

    //UI control variables
    private EditText edTxtUserNeedDescription;
    private EditText edTxtUserDocumentNumber;
    private Spinner spnNeeds;
    private String[] strNeeds;
    private String[] strCodeNeeds;
    private String strUserDocNumber;
    private int spnOptionSelected;
    private String UserValidated;
    private String Latitude = "0";
    private String Longitude = "0";
    private boolean NeedRegistered = false;

    //Error handler
    private boolean exc = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_needs);
            edTxtUserNeedDescription = (EditText) findViewById(R.id.edTxtNeedsDescription);
            edTxtUserDocumentNumber = (EditText) findViewById(R.id.edTxtDocNumberNeeds);

            //AsyncTask class to download Needs
            DownloadNeeds dNeeds = new DownloadNeeds();
            dNeeds.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Download needs
    private class DownloadNeeds extends AsyncTask<Void, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Needs.this);

        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando Información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            //Debug.waitForDebugger();
            //Todo
            //Invoke web method
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true; //Service is .net = true

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_NEEDS, envelope);

                //create SoapPrimitive and obtain response
                resultsRequestSOAP = (SoapObject)envelope.getResponse();

                if(resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();


                    if (intPropertyCount != 0) {
                        intPropertyCount = intPropertyCount + 1;
                        strNeeds = new String[intPropertyCount];
                        strCodeNeeds = new String[intPropertyCount];
                        strNeeds[0] = "Seleccione";
                        strCodeNeeds[0] = "0x";

                        for (int i = 0; i < intPropertyCount - 1; i++) {
                            //strNeeds[i]= resultsRequestSOAP.getPropertyAsString(i).toString();
                            //Format the information from the web service
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject needsList = (SoapObject) property;
                                strNeeds[i + 1] = needsList.getProperty("Descripcion").toString();
                                strCodeNeeds[i + 1] = needsList.getProperty("Codigo").toString();
                            }
                        }
                    } else {
                        intPropertyCount = +1;
                        strNeeds = new String[intPropertyCount];
                        strCodeNeeds = new String[intPropertyCount];
                        strNeeds[0] = "Seleccione";
                        strCodeNeeds[0] = "0x";
                    }
                }
                else {
                    exc = true;
                }
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            //android.os.Debug.waitForDebugger();
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(Needs.this,"Error de Conexion, Intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Needs.this.finish();
            }
            else {
                spinnerNeeds();
                exc = false;                
            }
        }
    }

    public void spinnerNeeds() {
        //Needs Spinner Control
        spnNeeds = (Spinner)findViewById(R.id.spnNeeds);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item, strNeeds);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnNeeds.setAdapter(adapter);
        spnNeeds.setSelection(0);

        spnNeeds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Todo
                spnOptionSelected = spnNeeds.getSelectedItemPosition();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void buttonOnClickRegistryNeed(View v)
    {
        if (strCodeNeeds[spnOptionSelected].toString().equals("0x"))
        {
            Toast toast = Toast.makeText(Needs.this,"Seleccione una necesidad de la lista", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if (edTxtUserNeedDescription.getText().length() < 1){
            Toast toast = Toast.makeText(Needs.this,"Debe registrar una descripción de la necesidad", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if(edTxtUserDocumentNumber.getText().length() <= 5){
            Toast toast = Toast.makeText(Needs.this,"Digite un documento valido", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else {
            strUserDocNumber = edTxtUserDocumentNumber.getText().toString();
            ValidateUser validateUser = new ValidateUser();
            validateUser.execute(strUserDocNumber);
        }
    }

    //Validate User
    private class ValidateUser extends AsyncTask<String, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(Needs.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Validando documento");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            //Debug.waitForDebugger();
            //Todo
            //Invoke web method
            try{
                request = new SoapObject(NAMESPACE, METHOD_NAME_USER_VALIDATION);
                request.addProperty("numeroCedula", params[0]);

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true; //Service is .net = true

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_VALIDATE_USER, envelope);

                //create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive)envelope.getResponse();
                UserValidated = resultRequestUserRegistrySOAP.toString();

            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            //Debug.waitForDebugger();
            if(this.dialog.isShowing()){
                this.dialog.dismiss();
            }
            if(exc){
                Toast toast = Toast.makeText(Needs.this,"Error de Conexion, Intente de nuevo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Needs.this.finish();
            }
            else {
                exc = false;
                if(UserValidated.equals("OK")){
                    RegistryUserNeed registryUserNeed = new RegistryUserNeed();
                    registryUserNeed.execute();
                }
                else if (UserValidated.equals("NO")){
                    Intent mainIntent = new Intent(Needs.this, SaveNeed.class);
                    mainIntent.putExtra("Necesidad", strNeeds[spnOptionSelected].toString());
                    mainIntent.putExtra("Codigo", strCodeNeeds[spnOptionSelected].toString());
                    mainIntent.putExtra("Descripcion",edTxtUserNeedDescription.getText().toString());
                    mainIntent.putExtra("Cedula", strUserDocNumber);
                    Needs.this.startActivity(mainIntent);
                    Needs.this.finish();
                }
                else {
                    //error handler when databse do not respond
                    Toast toast = Toast.makeText(Needs.this,"No hay acceso al servidor", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }

    //Registry User Need
    private class RegistryUserNeed extends AsyncTask<Void,Void,Void> {

        private final ProgressDialog dialog = new ProgressDialog(Needs.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Almacenando Necesidad");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Debug.waitForDebugger();
            try {

                //Invoke web method
                request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE_NEED);

                request.addProperty("codigoNecesidad", strCodeNeeds[spnOptionSelected].toString());
                request.addProperty("observacion", edTxtUserNeedDescription.getText().toString());
                request.addProperty("latitud", Latitude);
                request.addProperty("longitud", Longitude);
                request.addProperty("cedula", strUserDocNumber);

                envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;

                envelope.setOutputSoapObject(request);

                HttpTransportSE transportSE = new HttpTransportSE(URL);

                //Web service call
                transportSE.call(SOAP_ACTION_SAVE_NEED, envelope);
                //Create SoapPrimitive and obtain response
                resultRequestUserRegistrySOAP = (SoapPrimitive) envelope.getResponse();
                NeedRegistered = Boolean.parseBoolean(String.valueOf(resultRequestUserRegistrySOAP));

            } catch (Exception e) {
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (exc) {
                Toast toast = Toast.makeText(Needs.this,"Error de Conexion, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Needs.this.finish();
            } else {
                exc = false;
                if(NeedRegistered){
                    Toast toast = Toast.makeText(Needs.this,"Necesidad Almacenada correctamente", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Needs.this.finish();
                }
                else{
                    Toast toast = Toast.makeText(Needs.this,"No ha sido posible registrar la necesidad, confirme los datos y su conexión a internet", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    Needs.this.finish();
                }
            }
        }
    }

    public void buttonOnClickCancelNeed(View v)
    {
        Needs.this.finish();
    }
}

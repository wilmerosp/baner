package com.agroton.incoder.baner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class SeeNeeds extends Activity {

    //WS invocation constants
    private static final String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://186.154.209.213:3605/ServicioNecesidad.asmx";

    //For User Needs WS
    private static final String METHOD_NAME_MY_NEEDS = "ObtenerMisNecesidades";
    private static final String SOAP_ACTION_BRING_USER_NEEDS = "http://tempuri.org/ObtenerMisNecesidades";

    //Variable declaration to consume WS
    private SoapObject request = null;
    private SoapSerializationEnvelope envelope = null;
    private SoapObject resultsRequestSOAP = null;

    //UI control variables
    private ListView lstVwNeeds;
    //For Document Types
    private EditText edTxtUserDocTypeSeeNeeds;
    //For UserNeeds
    private String[] strNeedsDescription;
    private String[] strNeedStatus;
    private String[] strUserDocument;
    private String[] strIncoderAnswer;
    private String[] strNeedName;

    private int optListSelected;
    private boolean haveNeeds;
    private boolean exc = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_see_needs);

            edTxtUserDocTypeSeeNeeds = (EditText) findViewById(R.id.edTxtDocNumberSeeNeeds);
            lstVwNeeds = (ListView) findViewById(R.id.ltVwSeeNeeds);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    //Bring Needs depending on the Document
    private class DownloadUserNeeds extends AsyncTask<String,Void,Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(SeeNeeds.this);
        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Cargando información");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            //Debug.waitForDebugger();
            //Todo
            //Invoke web method
            request = new SoapObject(NAMESPACE, METHOD_NAME_MY_NEEDS);

            //Propertyinfo
            PropertyInfo userNeedsPI = new PropertyInfo();
            //Property Name
            userNeedsPI.setName("cedula");
            //Property Value
            userNeedsPI.setValue(params[0]);
            //Property Datatype
            userNeedsPI.setType(String.class);
            //Add property to request
            request.addProperty(userNeedsPI);

            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transportSE = new HttpTransportSE(URL);
            try{
                //Web service call
                transportSE.call(SOAP_ACTION_BRING_USER_NEEDS, envelope);
                //Create SoapPrimitive and obtain response
                resultsRequestSOAP= (SoapObject)envelope.getResponse();
                if (resultsRequestSOAP != null) {
                    int intPropertyCount = resultsRequestSOAP.getPropertyCount();

                    if (intPropertyCount > 0) {
                        haveNeeds = true;
                        strUserDocument = new String[intPropertyCount];
                        strNeedStatus = new String[intPropertyCount];
                        strNeedsDescription = new String[intPropertyCount];
                        strIncoderAnswer = new String[intPropertyCount];
                        strNeedName = new String[intPropertyCount];

                        for (int i = 0; i < intPropertyCount; i++) {
                            Object property = resultsRequestSOAP.getProperty(i);
                            if (property instanceof SoapObject) {
                                SoapObject NeedsList = (SoapObject) property;
                                //CedulaUsuario
                                strUserDocument[i] = NeedsList.getProperty("CedulaUsuario").toString();
                                //Observacion
                                strNeedsDescription[i] = NeedsList.getProperty("Observacion").toString();
                                //Estado
                                strNeedStatus[i] = NeedsList.getProperty("Estado").toString();
                                //Respueta
                                if (NeedsList.getProperty("Respueta").toString().equals("anyType{}")) {
                                    strIncoderAnswer[i] = "Necesidad en tramite";
                                } else {
                                    strIncoderAnswer[i] = NeedsList.getProperty("Respueta").toString();
                                }
                                //NombreNecesidad
                                strNeedName[i] = NeedsList.getProperty("NombreNecesidad").toString();
                            }
                        }
                    } else haveNeeds = false;
                }else exc = true;//No information from WS side
            }catch (Exception e){
                exc = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(this.dialog.isShowing())
            {
                this.dialog.dismiss();
            }
            if(exc){
                lstVwNeeds.setAdapter(null);
                Toast toast = Toast.makeText(SeeNeeds.this,"Error de Conexion, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                //SeeNeeds.this.finish();
            }
            else{
                exc = false;
                if (haveNeeds){
                    Toast toast = Toast.makeText(SeeNeeds.this,"Seleccione una opción de la lista para verla en detalle", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    populateListView();
                }
                else {
                    lstVwNeeds.setAdapter(null);
                    Toast toast = Toast.makeText(SeeNeeds.this,"No se encuentra información asociada a este documento", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }

    //Fill ListView
    private void populateListView(){

        ListAdapter listAdapter = new ArrayAdapter<String>(SeeNeeds.this, R.layout.perzonalicedlistview, strNeedName);
        lstVwNeeds.setAdapter(listAdapter);

        lstVwNeeds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mainIntent = new Intent(SeeNeeds.this, SeeNeedsDescription.class);
                mainIntent.putExtra("Necesidad", strNeedName[position].toString());
                mainIntent.putExtra("Descripcion",strNeedsDescription[position].toString());
                mainIntent.putExtra("Respuesta", strIncoderAnswer[position].toString());
                SeeNeeds.this.startActivity(mainIntent);
            }
        });
    }

    public void buttonOnClickSearchNeeds(View v){
        if(edTxtUserDocTypeSeeNeeds.getText().toString().length() < 5)
        {
            Toast toast = Toast.makeText(SeeNeeds.this,"Por favor indique un numero de documento valido", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else
        {
            DownloadUserNeeds dwUserNeeds = new DownloadUserNeeds();
            dwUserNeeds.execute(edTxtUserDocTypeSeeNeeds.getText().toString());
        }
    }

    public void buttonOnClickCancelSeeNeed(View v)
    {
        SeeNeeds.this.finish();
    }
}

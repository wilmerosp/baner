package com.agroton.incoder.baner;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class SeeNeedsDescription extends Activity {

    //UI Control variables
    private TextView txtVwNeed;
    private TextView txtDescription;
    private TextView txtVwAnser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_needs_description);

        txtVwNeed = (TextView)findViewById(R.id.txtVwChoosenNeedSeeNeeds);
        txtDescription = (TextView)findViewById(R.id.txtVwNeedDescriptionSeeNeeds);
        txtVwAnser = (TextView)findViewById(R.id.txtVwAnswerSeeNeeds);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                txtVwNeed.setText(extras.getString("Necesidad"));
                txtDescription.setText(extras.getString("Descripcion"));
                txtVwAnser.setText(extras.getString("Respuesta"));
            }
            else {
                Toast toast = Toast.makeText(SeeNeedsDescription.this,"No se recibió información, intente nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void buttonOnClickCancelSeeNeedDescription(View v)
    {
        SeeNeedsDescription.this.finish();
    }

}
